all: baselines viterbi forward_backward

.PHONY: baselines viterbi forward_backward

#data/europarl:
#	make -C fsts

baselines:
	make -C baselines install

baselines:
	make -C viterbi install

forward_backward:
	make -C forward_backward
