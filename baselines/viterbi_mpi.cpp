// Serial implementation of the Viterbi algorithm

#include "mpi.h"
#include <vector>
#include <tuple>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <chrono>

#include <cmath>
#include <cfloat>
#include <cassert>

#include "numberizer.hpp"
#include "fst.hpp"

using namespace std;

// FST whose transitions are indexed by input symbol.
struct input_fst {
  state_t initial;
  std::vector<int> input_offsets;
  std::vector<std::tuple<state_t, state_t, sym_t, prob_t>> transitions;
  std::vector<std::tuple<state_t, prob_t>> finals;
  state_t num_states;
  sym_t num_inputs, num_outputs;

  input_fst (fst &&m) 
    : 
    initial(m.initial), 
    num_states(m.num_states), 
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    input_offsets(m.num_inputs+1),
    transitions(m.transitions.size()) 
  {
    //std::sort(m.transitions.begin(), m.transitions.end(), compare_input);
    std::sort(m.transitions.begin(), m.transitions.end(), compare_input_fromstate_tostate);
    sym_t f_last = -1;
    for (int i=0; i<m.transitions.size(); i++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];
      while (f > f_last) {
	f_last++;
	input_offsets[f_last] = i;
      }
      transitions[i] = std::make_tuple(q, r, e, p);
    }
    input_offsets.back() = m.transitions.size();
    std::swap(finals, m.finals);
  }
};

prob_t viterbi(const input_fst &m, const vector<sym_t> &input_symbols, vector<sym_t> &output_symbols) {
    int verbose = 0;
    state_t q, r;
    sym_t f, e;
    prob_t p;

    static vector<vector<prob_t>> viterbi;
    static vector<vector<int>> backpointer;

    int n = input_symbols.size();
    if (n > viterbi.size()) viterbi.resize(n);
    if (n > backpointer.size()) backpointer.resize(n);

    for (int t=0; t<n; t++) {
      viterbi[t].resize(m.num_states);
      backpointer[t].resize(m.num_states);
      fill(viterbi[t].begin(), viterbi[t].end(), -FLT_MAX);
      fill(backpointer[t].begin(), backpointer[t].end(), -1);
    }

    // Special handling of first symbol

    if (verbose >= 2) cout << "word 0" << endl;
    f = input_symbols[0];
    for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
      tie(q, r, e, p) = m.transitions[k];
      if (q != m.initial) continue;
      if (p > viterbi[0][r]) {
	viterbi[0][r] = p;
	backpointer[0][r] = k;
      }
    }

    for (int t=1; t<n; t++) {
      if (verbose >= 2) cout << "word " << t << endl;
      f = input_symbols[t];
      for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
	tie(q, r, e, p) = m.transitions[k];
	if (backpointer[t-1][q] == -1) continue;
	prob_t new_prob = viterbi[t-1][q] + p;
	if (new_prob > viterbi[t][r]) {
	  viterbi[t][r] = new_prob;
	  backpointer[t][r] = k;
        }
      }
    }
	
    state_t final_state = -1;
    prob_t final_prob = -FLT_MAX;
    for (const auto &final: m.finals) {
      tie(q, p) = final;
      prob_t new_prob = viterbi[n-1][q] + p;
      if (p > final_prob) {
	final_state = q;
	final_prob = new_prob;
      }
    }
    if (final_state == -1) {
      output_symbols.clear();
      return -FLT_MAX;
    }

    // RECONSTRUCT OUTPUT
    output_symbols.clear();
    q = final_state;
    for (int t=n-1; t>=0; t--) {
      tie(q, r, e, p) = m.transitions[backpointer[t][q]];
      output_symbols.push_back(e);
    }
    reverse(output_symbols.begin(), output_symbols.end());
    return final_prob;
}

int main (int argc, char *argv[]) {

  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  const input_fst m = read_fst(argv[1], inr, onr);


  vector<int> sources;//(m.input_offsets.size());
  vector<int> targets;//(m.input_offsets.size());
  vector<float> probs;//(m.input_offsets.size());
  vector<int> out_labels;//(m.input_offsets.size());

  vector<vector<prob_t>> viterbi_array;
  vector<vector<int>> backpointer_array; 
  MPI_Status status;
  MPI_Init(NULL, NULL);
  // Get the number of processes
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Get the rank of the process
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);
  // Print off a hello world message
  printf("Hello world from processor %s, rank %d"
           " out of %d processors\n",
           processor_name, world_rank, world_size);
  int q, r;
  int f, e;
  float p;
  sources.resize(m.transitions.size());
  targets.resize(m.transitions.size());
  probs.resize(m.transitions.size());
  out_labels.resize(m.transitions.size());


  for (int k=0; k<m.transitions.size(); k++){
    tie(q, r, e, p) = m.transitions[k];
    sources[k] = q;
    targets[k] = r;
    out_labels[k] = e;
    probs[k] = p;
  }

  auto clock1 = std::chrono::steady_clock::now();
  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;



  for (int iteration=0; iteration<1000; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      auto read_start = std::chrono::steady_clock::now();
      input_symbols = inr.split(line);

      if(world_rank == 0){
        cout << "Input: " << inr.join(input_symbols) << " " << iteration << endl;
      }

      int n = input_symbols.size();
      if (n > viterbi_array.size()) viterbi_array.resize(n);
      if (n > backpointer_array.size()) backpointer_array.resize(n);

      for (int t=0; t<n; t++) {
        viterbi_array[t].resize(m.num_states);
        backpointer_array[t].resize(m.num_states);
        fill(viterbi_array[t].begin(), viterbi_array[t].end(), -FLT_MAX);
        fill(backpointer_array[t].begin(), backpointer_array[t].end(), -1);
      }

      f = input_symbols[0];
      for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
        tie(q, r, e, p) = m.transitions[k];
        if (q != m.initial) continue;
        if (p > viterbi_array[0][r]) {
          viterbi_array[0][r] = p;
          backpointer_array[0][r] = k;
        }
      }

  int verbose = 0;
  for (int t=1; t<n; t++) {
    if (verbose >= 2) cout << "word " << t << endl;
    f = input_symbols[t];

    int start_index = m.input_offsets[f]; 
    int end_index = m.input_offsets[f+1];
    int num_elements = m.input_offsets[f+1] - m.input_offsets[f];
    int offset_amount = (num_elements + (world_size-1) -1) / (world_size-1);

    vector<int> proc_sizes(world_size-1);
    int elem_avail = num_elements;
    for(int foo=0;foo<world_size-1;foo++){
      if(elem_avail > 0){
	if(elem_avail < offset_amount){
	  proc_sizes[foo] = elem_avail;
	}
	else{
	  proc_sizes[foo] = offset_amount;
	}
	elem_avail -= offset_amount;
      }
      else{
        proc_sizes[foo] = 0;
      }
    }

    vector<int> proc_offsets(world_size-1);
    int offset_sum = 0;
    for(int foo=0;foo<world_size-1;foo++){
      proc_offsets[foo] = offset_sum;
      offset_sum+=proc_sizes[foo];
      //printf("offset size [%d] is %d and sum %d elements %d\n",foo,proc_sizes[foo],proc_offsets[foo],num_elements);
    }

    // Split the work among "processors" number of processors
 
    vector<float> viterbi_array_tmp;
    viterbi_array_tmp.resize(m.num_states);
    vector<int> backpointer_array_tmp;
    backpointer_array_tmp.resize(m.num_states);
    
    int size_list = 0;
    if(world_rank == 0){
      for(int num_proc = 1 ; num_proc< world_size; num_proc++){
          //int MPI_Send(const void *buf, int count, MPI_Datatype datatype, int dest, int tag,MPI_Comm comm)

          MPI_Send(&t,1,MPI_INT,num_proc,1,MPI_COMM_WORLD);

          MPI_Send(&viterbi_array[t-1][0], m.num_states,MPI_FLOAT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&viterbi_array[t][0], m.num_states,MPI_FLOAT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&backpointer_array[t][0],m.num_states,MPI_INT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&backpointer_array[t-1][0],m.num_states,MPI_INT,num_proc,1,MPI_COMM_WORLD);

          MPI_Send(&start_index,1,MPI_INT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&end_index,1,MPI_INT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&offset_amount,1,MPI_INT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&proc_offsets[0],world_size-1,MPI_INT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&proc_sizes[0],world_size-1,MPI_INT,num_proc,1,MPI_COMM_WORLD);

          //printf("OFFSET AMOUNT %d size %d and index %d \n",offset_amount,proc_sizes[(num_proc-1)],proc_offsets[(num_proc-1)]); 
          MPI_Send(&sources[0] + (start_index + proc_offsets[(num_proc-1)]), proc_sizes[(num_proc-1)] ,MPI_INT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&targets[0] + (start_index + proc_offsets[(num_proc-1)]), proc_sizes[(num_proc-1)] ,MPI_INT,num_proc,1,MPI_COMM_WORLD);
          MPI_Send(&probs[0] + (start_index + proc_offsets[(num_proc-1)]), proc_sizes[(num_proc-1)] ,MPI_FLOAT,num_proc,1,MPI_COMM_WORLD);


      }


      for(int num_proc = 1 ; num_proc< world_size; num_proc++){
          //int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status *status)
          MPI_Recv(&viterbi_array_tmp[0], m.num_states,MPI_FLOAT,num_proc, 2, MPI_COMM_WORLD,&status);
          MPI_Recv(&backpointer_array_tmp[0], m.num_states,MPI_INT,num_proc, 2, MPI_COMM_WORLD,&status);

          // Perform the max operation on the viterbi array
          for(int rec_pointer = 0; rec_pointer < m.num_states ; rec_pointer++){
              if(viterbi_array[t][rec_pointer] < viterbi_array_tmp[rec_pointer]){
                  viterbi_array[t][rec_pointer] = viterbi_array_tmp[rec_pointer];
                  backpointer_array[t][rec_pointer] = backpointer_array_tmp[rec_pointer];
              }
          }
      }


    }
    if(world_rank > 0){
        MPI_Recv(&t,1,MPI_INT,0,1, MPI_COMM_WORLD,&status);
        MPI_Recv(&viterbi_array[t-1][0], m.num_states,MPI_FLOAT,0, 1, MPI_COMM_WORLD,&status);
        MPI_Recv(&viterbi_array[t][0], m.num_states,MPI_FLOAT,0, 1, MPI_COMM_WORLD,&status);
        MPI_Recv(&backpointer_array[t][0], m.num_states,MPI_INT,0, 1, MPI_COMM_WORLD,&status);
        MPI_Recv(&backpointer_array[t-1][0], m.num_states,MPI_INT,0, 1, MPI_COMM_WORLD,&status);

        MPI_Recv(&start_index,1,MPI_INT,0,1, MPI_COMM_WORLD,&status);
        MPI_Recv(&end_index,1,MPI_INT,0,1, MPI_COMM_WORLD,&status);
        MPI_Recv(&offset_amount,1,MPI_INT,0,1, MPI_COMM_WORLD,&status);
        MPI_Recv(&proc_offsets[0],world_size-1,MPI_INT,0,1,MPI_COMM_WORLD,&status);
        MPI_Recv(&proc_sizes[0],world_size-1,MPI_INT,0,1,MPI_COMM_WORLD,&status);

        MPI_Recv(&sources[0] + (start_index + proc_offsets[world_rank-1]), proc_sizes[world_rank-1], MPI_INT,0, 1, MPI_COMM_WORLD,&status);
        MPI_Recv(&targets[0] + (start_index + proc_offsets[world_rank-1]), proc_sizes[world_rank-1], MPI_INT,0, 1, MPI_COMM_WORLD,&status);
        MPI_Recv(&probs[0] + (start_index + proc_offsets[world_rank-1]), proc_sizes[world_rank-1], MPI_FLOAT,0, 1, MPI_COMM_WORLD,&status);


	int start_iter = start_index + proc_offsets[world_rank-1];
        int end_iter = start_iter + proc_sizes[world_rank-1];

        for(start_iter ; start_iter < end_iter; start_iter++){
	  if (backpointer_array[t-1][sources[start_iter]] != -1){
	    float new_prob = viterbi_array[t-1][sources[start_iter]] + probs[start_iter];

	    if(new_prob > viterbi_array[t][targets[start_iter]]){
              viterbi_array[t][targets[start_iter]] = new_prob;
	      backpointer_array[t][targets[start_iter]] = start_iter;
	    }

	  }

        }


        MPI_Send(&viterbi_array[t][0],  m.num_states, MPI_FLOAT, 0, 2, MPI_COMM_WORLD);
        MPI_Send(&backpointer_array[t][0],  m.num_states, MPI_INT, 0, 2, MPI_COMM_WORLD);

    }
    //MPI_Barrier(MPI_COMM_WORLD);
  }

    state_t final_state = -1;
    prob_t final_prob = -FLT_MAX;
    for (const auto &final: m.finals) {
      tie(q, p) = final;
      prob_t new_prob = viterbi_array[n-1][q] + p;
      if (p > final_prob) {
        final_state = q;
        final_prob = new_prob;
      }
    } 
  

    output_symbols.clear();
    q = final_state;
    for (int t=n-1; t>=0; t--) {
      tie(q, r, e, p) = m.transitions[backpointer_array[t][q]];
      output_symbols.push_back(e);
    }
    reverse(output_symbols.begin(), output_symbols.end());
    if(world_rank > 0){
      cout << "Viterbi log-probability: " << final_prob << endl;
      cout << "Viterbi output: " << onr.join(output_symbols) << endl;
      auto read_end = std::chrono::steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }
  }

  if(world_rank > 0){
    auto clock3 = std::chrono::steady_clock::now();
    diff = clock3-clock2;
    cout << "Time to decode sentences: " << diff.count() << endl;
  }

  MPI_Finalize();
  return 0;
}
