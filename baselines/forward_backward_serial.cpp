// Serial implementation of the Viterbi algorithm

#include <vector>
#include <tuple>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <chrono>

#include <cmath>
#include <cfloat>
#include <cassert>

#include "numberizer.hpp"
#include "fst.hpp"

using namespace std;

// FST whose transitions are indexed by input symbol.
struct input_fst {
  state_t initial;
  std::vector<int> input_offsets;
  std::vector<std::tuple<state_t, state_t, sym_t, prob_t>> transitions;
  std::vector<std::tuple<state_t, prob_t>> finals;
  state_t num_states;
  sym_t num_inputs, num_outputs;

  input_fst (fst &&m) 
    : 
    initial(m.initial), 
    num_states(m.num_states), 
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    input_offsets(m.num_inputs+1),
    transitions(m.transitions.size()) 
  {
    std::sort(m.transitions.begin(), m.transitions.end(), compare_input);
    sym_t f_last = -1;
    for (int i=0; i<m.transitions.size(); i++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];
      while (f > f_last) {
	f_last++;
	input_offsets[f_last] = i;
      }
      transitions[i] = std::make_tuple(q, r, e, p);
    }
    input_offsets.back() = m.transitions.size();
    std::swap(finals, m.finals);
  }
};

float logadd(float x, float y) {
  if (x < y) swap(x, y);
  return x + log1p(exp(y-x));
}

prob_t forward(const input_fst &m, const vector<sym_t> &input_symbols) {
    int verbose = 0;
    state_t q, r;
    sym_t f, e;
    prob_t p;

    int n = input_symbols.size();
    vector<vector<prob_t>> forward(n);
    vector<vector<prob_t>> backward(n);
    vector<vector<prob_t>> expected_counts(n);
    for (int t=0; t<n; t++){
      forward[t].resize(m.num_states, -FLT_MAX);
      backward[t].resize(m.num_states, -FLT_MAX);
      expected_counts[t].resize(m.num_states, -FLT_MAX);
    }
    // Special handling of first symbol

    if (verbose >= 2) cout << "word 0" << endl;
    f = input_symbols[0];
    for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
      tie(q, r, e, p) = m.transitions[k];
      if (q != m.initial) continue;
      forward[0][r] = logadd(forward[0][r], p);
    }

    for (int t=1; t<n; t++) {
      if (verbose >= 2) cout << "word " << t << endl;
      f = input_symbols[t];
      for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
	tie(q, r, e, p) = m.transitions[k];
	forward[t][r] = logadd(forward[t][r], forward[t-1][q] + p);
      }
    }
	
    prob_t final_prob = -FLT_MAX;
    for (const auto &final: m.finals) {
      tie(q, p) = final;
      final_prob = logadd(final_prob, forward[n-1][q] + p);
    }
    
    // Do backward pass here 
    f = input_symbols[n-1];
    state_t end_state = std::get<0>(m.finals[0]);
    for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
      tie(q, r, e, p) = m.transitions[k];
      if (r != end_state){continue;}
      backward[n-1][q] = logadd(backward[n-1][q], p);
    }

    for (int t=n-2; t>=0; t--) {
        if (verbose >= 2) cout << "word " << t << endl;
        f = input_symbols[t];
        for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
            tie(q, r, e, p) = m.transitions[k];
            if(backward[t+1][r] > -FLT_MAX){
                backward[t][q] = logadd(backward[t][q], backward[t+1][r] + p);
            }
        }
    }

    for (int t=0; t>=n; t++) {
        if (verbose >= 2) cout << "word " << t << endl;
        f = input_symbols[t];
        for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
            tie(q, r, e, p) = m.transitions[k];
            if(backward[t+1][r] > -FLT_MAX){
                expected_counts[t][q] = (forward[t][r] + backward[t][r] + p)-final_prob;
            }
        }
    }
    
    return final_prob;
}

int main (int argc, char *argv[]) {
  auto clock1 = std::chrono::steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  const input_fst m = read_fst(argv[1], inr, onr);
  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;

  for (int iteration=0; iteration<10; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      prob_t final_prob = forward(m, input_symbols);
      //cout << "Forward log-probability: " << final_prob << endl;
    }
  }
  auto clock3 = std::chrono::steady_clock::now();
  diff = clock3-clock2;
  cout << "Time to decode sentences: " << diff.count() << endl;

  return 0;
}
