// Forward algorithm using cuSPARSE library.
// Bugs:
// - doesn't handle multiple edges (one might hope that cuSPARSE does, but it doesn't)

// -*- mode: c++ -*-

#include <tuple>
#include <vector>
#include <map>
#include <chrono>
#include <algorithm>

#include <iostream>
#include <fstream>
#include <sstream>

#include <cfloat>
#include <cstdlib>

#include <cuda_runtime.h>
#include <cusparse.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_utils.hpp"

using namespace std;

cusparseHandle_t cusparse_handle;

template<class T>
struct exp_functor {
  __device__ T operator()(T &x) const {
    return exp(x);
  }
};

template<class T>
struct div_functor {
  T y;
  div_functor(T y) : y(y) { }
  __device__ T operator()(T &x) const {
    return x/y;
  }
};

// FST whose transitions are indexed by input symbol, then "from" state
struct gpu_input_csr_fst {
  // This is just a big CSR matrix. The transitions for (q, r, f, *, *) are found
  // at M[f*num_states+q][r].
  state_t initial;
  thrust::device_vector<int> row_offsets;
  thrust::device_vector<state_t> to_states;
  thrust::device_vector<prob_t> probs;
  thrust::device_vector<state_t> final_states;
  thrust::device_vector<prob_t> final_probs;
  state_t num_states;
  sym_t num_inputs, num_outputs;

  gpu_input_csr_fst (fst &&m) 
    : 
    initial(m.initial), 
    num_states(m.num_states), 
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    to_states(m.transitions.size()),
    probs(m.transitions.size()),
    final_states(m.finals.size()),
    final_probs(m.finals.size())
  {
    std::vector<int> h_row_offsets(num_inputs*num_states+1);

    // workaround for bug in nvcc
    sort_by_input_fromstate_tostate(m);
    //std::sort(m.transitions.begin(), m.transitions.end(), compare_input_fromstate_tostate);

    int i = -1;
    for (int k=0; k<m.transitions.size(); k++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[k];
      while (i < f*num_states+q) {
	i++;
	h_row_offsets[i] = k;
      }
    }
    while (i < num_inputs*num_states) {
      i++;
      h_row_offsets[i] = m.transitions.size();
    }
    row_offsets = h_row_offsets;

    // Unzip m.transitions into to_states and probs
    unzip_to_device<1>(m.transitions, to_states);
    unzip_to_device<4>(m.transitions, probs);
    thrust::transform(probs.begin(), probs.end(), probs.begin(), exp_functor<prob_t>());

    // Unzip m.finals into final_states and final_probs
    unzip_to_device<0>(m.finals, final_states);
    unzip_to_device<1>(m.finals, final_probs);
    thrust::transform(final_probs.begin(), final_probs.end(), final_probs.begin(), exp_functor<prob_t>());
  }
};

prob_t forward_cusparse(const gpu_input_csr_fst &m, const vector<sym_t> &input_symbols) {
  const int verbose = 0;
  // Initial vector
  thrust::device_vector<prob_t> forward(m.num_states, 0);
  printf("The initial index is: %d \n",int(m.initial));
  forward[m.initial] = 1;
  thrust::device_vector<prob_t> forward_tmp(m.num_states);
  if (verbose) {
    cerr << "forward probabilities" << endl;
    for (const auto &p: forward)
      cerr << p << endl;
  }

  cusparseMatDescr_t descr;
  cusparseCreateMatDescr(&descr);
  cusparseSetMatType(descr, CUSPARSE_MATRIX_TYPE_GENERAL); 
  cusparseSetMatIndexBase(descr, CUSPARSE_INDEX_BASE_ZERO);

  float e = 0;

  for (int t=0; t<input_symbols.size(); t++) {
    if (verbose)
      cerr << "word " << t << endl;
    sym_t f = input_symbols[t];
    int nnz = m.row_offsets[(f+1)*m.num_states] - m.row_offsets[f*m.num_states];
    //nnz = int(m.to_states.size());
    const int *mu_row_offsets = m.row_offsets.data().get() + f*m.num_states;
    const prob_t *mu_values = m.probs.data().get();
    const int *mu_cols = m.to_states.data().get();
    const float alpha = 1, beta = 0;

    if (1) {
      cerr << "transition matrix" << endl;
      for (int i=0; i<m.num_states; i++) {
	cerr << "row " << i << " at offset " << m.row_offsets[f*m.num_states+i] << endl;
	for (int k=m.row_offsets[f*m.num_states+i]; k<m.row_offsets[f*m.num_states+i+1]; k++) {
	  cerr << "  col " << m.to_states[k] << " val " << m.probs[k] << endl;
	}
      }
      cerr << "last row ends at offset " << m.row_offsets[(f+1)*m.num_states] << endl;
    }

    cusparseScsrmv(cusparse_handle, 
		   CUSPARSE_OPERATION_TRANSPOSE,
		   m.num_states, m.num_states, nnz,
		   &alpha,
		   descr, mu_values, mu_row_offsets, mu_cols,
		   forward.data().get(),
		   &beta,
		   forward_tmp.data().get());

    //print all the values for forward temp
    for(int foo = 0; foo< m.num_states; foo++){
        if(float(forward_tmp[foo]) > 0){
            printf("[%d] = %.10e\n",foo,float(forward_tmp[foo]));
        }
    }

    swap(forward, forward_tmp);

    if (verbose) {
      cerr << "forward probabilities" << endl;
      for (const auto &p: forward)
	cerr << p << endl;
    }

    // To avoid underflow, normalize
    prob_t s = thrust::reduce(forward.begin(), forward.end());
    thrust::transform(forward.begin(), forward.end(), forward.begin(), div_functor<prob_t>(s));
    e += log(s);

    if (verbose) {
      cerr << "forward probabilities" << endl;
      for (const auto &p: forward)
	cerr << p << endl;
      cerr << "scale: " << e << endl;
    }

  }

  prob_t result;
  cusparseSdoti(cusparse_handle,
		m.final_states.size(),
		m.final_probs.data().get(),
		m.final_states.data().get(),
		forward.data().get(),
		&result,
		CUSPARSE_INDEX_BASE_ZERO);

  if (verbose)
    cerr << "done" << endl;
  return log(result) + e;
}

int main (int argc, char *argv[]) {
  cusparseCreate(&cusparse_handle);

  auto clock1 = std::chrono::steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  gpu_input_csr_fst t = read_fst(argv[1], inr, onr);
  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;

  for (int iteration=0; iteration<1; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      auto read_start = std::chrono::steady_clock::now();
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      prob_t final_prob = forward_cusparse(t, input_symbols);
      cout << "Forward probability: " << final_prob << endl;
      auto read_end = std::chrono::steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }
  cudaDeviceSynchronize();
  auto clock3 = std::chrono::steady_clock::now();
  diff = clock3-clock2;
  cout << "Time to decode sentences: " << diff.count() << endl;

  return 0;
}
