// Serial implementation of the Viterbi algorithm

#include <vector>
#include <tuple>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <chrono>

#include <cmath>
#include <cfloat>
#include <cassert>

#include "numberizer.hpp"
#include "fst.hpp"

using namespace std;

// FST whose transitions are indexed by input symbol.
struct input_fst {
  state_t initial;
  std::vector<int> input_offsets;
  std::vector<std::tuple<state_t, state_t, sym_t, prob_t>> transitions;
  std::vector<std::tuple<state_t, prob_t>> finals;
  state_t num_states;
  sym_t num_inputs, num_outputs;

  input_fst (fst &&m) 
    : 
    initial(m.initial), 
    num_states(m.num_states), 
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    input_offsets(m.num_inputs+1),
    transitions(m.transitions.size()) 
  {
    //std::sort(m.transitions.begin(), m.transitions.end(), compare_input);
    std::sort(m.transitions.begin(), m.transitions.end(), compare_input_fromstate_tostate);
    sym_t f_last = -1;
    for (int i=0; i<m.transitions.size(); i++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];
      while (f > f_last) {
	f_last++;
	input_offsets[f_last] = i;
      }
      transitions[i] = std::make_tuple(q, r, e, p);
    }
    input_offsets.back() = m.transitions.size();
    std::swap(finals, m.finals);
  }
};

prob_t viterbi(const input_fst &m, const vector<sym_t> &input_symbols, vector<sym_t> &output_symbols) {
    int verbose = 0;
    state_t q, r;
    sym_t f, e;
    prob_t p;

    static vector<vector<prob_t>> viterbi;
    static vector<vector<int>> backpointer;

    int n = input_symbols.size();
    if (n > viterbi.size()) viterbi.resize(n);
    if (n > backpointer.size()) backpointer.resize(n);

    for (int t=0; t<n; t++) {
      viterbi[t].resize(m.num_states);
      backpointer[t].resize(m.num_states);
      fill(viterbi[t].begin(), viterbi[t].end(), -FLT_MAX);
      fill(backpointer[t].begin(), backpointer[t].end(), -1);
    }

    // Special handling of first symbol

    if (verbose >= 2) cout << "word 0" << endl;
    f = input_symbols[0];
    for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
      tie(q, r, e, p) = m.transitions[k];
      if (q != m.initial) continue;
      if (p > viterbi[0][r]) {
	viterbi[0][r] = p;
	backpointer[0][r] = k;
      }
    }

    for (int t=1; t<n; t++) {
      if (verbose >= 2) cout << "word " << t << endl;
      f = input_symbols[t];
      for (int k=m.input_offsets[f]; k<m.input_offsets[f+1]; k++) {
	tie(q, r, e, p) = m.transitions[k];
	if (backpointer[t-1][q] == -1) continue;
	prob_t new_prob = viterbi[t-1][q] + p;
	if (new_prob > viterbi[t][r]) {
	  viterbi[t][r] = new_prob;
	  backpointer[t][r] = k;
        }
      }
    }
	
    state_t final_state = -1;
    prob_t final_prob = -FLT_MAX;
    for (const auto &final: m.finals) {
      tie(q, p) = final;
      prob_t new_prob = viterbi[n-1][q] + p;
      if (p > final_prob) {
	final_state = q;
	final_prob = new_prob;
      }
    }
    if (final_state == -1) {
      output_symbols.clear();
      return -FLT_MAX;
    }

    // RECONSTRUCT OUTPUT
    output_symbols.clear();
    q = final_state;
    for (int t=n-1; t>=0; t--) {
      tie(q, r, e, p) = m.transitions[backpointer[t][q]];
      output_symbols.push_back(e);
    }
    reverse(output_symbols.begin(), output_symbols.end());
    return final_prob;
}

int main (int argc, char *argv[]) {
  auto clock1 = std::chrono::steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  const input_fst m = read_fst(argv[1], inr, onr);
  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;

  for (int iteration=0; iteration<1000; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      auto read_start = std::chrono::steady_clock::now();
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      prob_t final_prob = viterbi(m, input_symbols, output_symbols);
      cout << "Viterbi log-probability: " << final_prob << endl;
      cout << "Viterbi output: " << onr.join(output_symbols) << endl;
      auto read_end = std::chrono::steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }
  auto clock3 = std::chrono::steady_clock::now();
  diff = clock3-clock2;
  cout << "Time to decode sentences: " << diff.count() << endl;

  return 0;
}
