#include <fst/fstlib.h>
#include <fst/vector-fst.h>
#include <fst/string.h>

#include <chrono>   // std::chrono::seconds, milliseconds
#include <string>
#include <fstream>
#include <sstream>
#include <cfloat>
#include <cmath>

using namespace std;
using namespace fst;

vector<int> split(const SymbolTable &t, const string &s) {
  istringstream iss(s);
  vector<int> nums;
  string word;
  while (iss >> word)
    nums.push_back(t.Find(word));
  return nums;
}

string join(const SymbolTable &t, const vector<int> &nums) {
  ostringstream oss;
  bool first = true;
  for (int num: nums) {
    if (first)
      first = false;
    else
      oss << " ";
    oss << t.Find(num);
  }
  return oss.str();
}

StdVectorFst make_path(const vector<int> &syms) {
  StdVectorFst m;
  StdVectorFst::StateId q = m.AddState();
  m.SetStart(q);
  for (int s: syms) {
    StdVectorFst::StateId r = m.AddState();
    m.AddArc(q, StdArc(s, s, StdVectorFst::Weight::One(), r));
    q = r;
  }
  m.SetFinal(q, StdVectorFst::Weight::One());
  return m;
}

float viterbi_openfst(const StdVectorFst &model, const vector<int> &input_symbols, vector<int> &output_symbols) {
  auto input = make_path(input_symbols);
  //input.Write("input.fst");	
  StdComposeFst composed(input, model);
  //composed.Write("composed.fst");
  
  StdVectorFst best_path;
  ShortestPath(composed, &best_path);
  //best_path.Write("best_path.fst");	

  output_symbols.clear();
  StdVectorFst::Weight p = StdVectorFst::Weight::One();
  StdVectorFst::StateId q = best_path.Start();
  if (q < 0) {
    return -FLT_MAX;
  } else {
    while (1) {
      ArcIterator<StdVectorFst> aiter(best_path, q);
      if (aiter.Done()) break;
      const StdArc &arc = aiter.Value();
      output_symbols.push_back(arc.olabel);
      p = Times(p, arc.weight);
      q = arc.nextstate;
    }
  }
  return p.Value();
}

int main(int argc, char* argv[])
{
  if (argc != 3) {
    cout << "Usage: " << argv[0] << " fst-file input-file\n"; 
    exit(0);
  }
  StdVectorFst *model = StdVectorFst::Read(argv[1]);
  ArcSort(model, StdILabelCompare());
  for (StateIterator<StdVectorFst> siter(*model); !siter.Done(); siter.Next()) {
    for (MutableArcIterator<StdVectorFst> aiter(model, siter.Value()); !aiter.Done(); aiter.Next()) {
      StdArc arc = aiter.Value();
      arc.weight = -log(arc.weight.Value());
      aiter.SetValue(arc);
    }
  }
  std::chrono::duration<float> diff;
  auto start_wall_clock = std::chrono::steady_clock::now();

  vector<int> output_symbols;
  for (int iteration=0; iteration<1; iteration++) {
    ifstream inputFile(argv[2]);
    string line;
    while (getline(inputFile, line)) {
      auto read_start = std::chrono::steady_clock::now();
      cout << "Input: " << line << endl;
      float p = viterbi_openfst(*model, split(*model->InputSymbols(), line), output_symbols);
      cout << "Viterbi log-probability: " << p << endl;
      cout << "Viterbi output: " << join(*model->OutputSymbols(), output_symbols) << endl;
      auto read_end = std::chrono::steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }
  
  auto finish_wall_clock = std::chrono::steady_clock::now();
  diff = (finish_wall_clock - start_wall_clock);
  std::cout << "time to decode sentences: " << diff.count() << '\n';
  
  
  delete model;
  
  return 0;
}
