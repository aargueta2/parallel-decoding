// Forward algorithm using MKL library.

// -*- mode: c++ -*-

#include <tuple>
#include <vector>
#include <map>
#include <chrono>
#include <algorithm>

#include <iostream>
#include <fstream>
#include <sstream>

#include <cfloat>
#include <cstdlib>

#include "numberizer.hpp"
#include "fst.hpp"
#include "mkl.h"

using namespace std;
using namespace std::chrono;


struct input_fst {

  state_t initial;
  MKL_INT *row_offsets;
  MKL_INT *to_states;
  double *probs;
  MKL_INT *final_states;
  double *final_probs;
  MKL_INT num_states;
  MKL_INT num_inputs, num_outputs;

  input_fst (fst &&m)
    :
    initial(m.initial),
    num_states(m.num_states),
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs)
 {
    row_offsets = (int *)mkl_malloc( int(num_inputs*num_states+1)*sizeof( int ), 64 );
    to_states = (int *)mkl_malloc( int(m.transitions.size())*sizeof( int ), 64 );
    probs = (double *)mkl_malloc( int(m.transitions.size())*sizeof( double ), 64 );
    final_states =  (int *)mkl_malloc( int(m.finals.size())*sizeof( int ), 64 );
    final_probs = (double *)mkl_malloc( int(m.finals.size())*sizeof( double ), 64 );
   

    //std::sort(m.transitions.begin(), m.transitions.end(), compare_input);
    sort_by_input_fromstate_tostate(m);


    int i = -1;
    for (int k=0; k<m.transitions.size(); k++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[k];
      to_states[k] = r;
      probs[k] = exp(p);
      while (i < f*num_states+q) {
        i++;
        row_offsets[i] = k;
      }
    }
    while (i < num_inputs*num_states) {
      i++;
      row_offsets[i] = m.transitions.size();
    }


    for(int l =0; l<m.finals.size(); l++){
        state_t q;
        prob_t p;
        std::tie(q, p) = m.finals[l];
        final_states[l] = q;
        final_probs[l] = exp(p);
    }
  }
};

prob_t forward_mkl(const input_fst &m, const vector<sym_t> &input_symbols/*, float *fst_table*/) {
  const int verbose = 0;
  // Initial vector

  double *forward;
  forward = ( double *)mkl_malloc( m.num_states*sizeof(double ), 64 );
  forward[m.initial] = 1;
  double *forward_tmp;
  forward_tmp = ( double *)mkl_malloc( m.num_states*sizeof( double ), 64 );


  double alpha, beta;
  alpha = 1.0; beta = 0.0;
  MKL_INT m2, n, k;
  m2 = n = k = m.num_states;
  float e = 0;
  char transa = 'N';
  char matdescra[6];

  for (int t=0; t<input_symbols.size(); t++) {

    if (verbose)
      cerr << "word " << t << endl;

    sym_t f = input_symbols[t];
    MKL_INT nnz = m.row_offsets[(f+1)*m.num_states] - m.row_offsets[f*m.num_states];
    MKL_INT *mu_row_offsets = m.row_offsets + f*m.num_states;
    double *mu_values = m.probs;
    MKL_INT *mu_cols = m.to_states;

    matdescra[0] = 'g';
    matdescra[1] = 'l';
    matdescra[2] = 'n';
    matdescra[3] = 'c';


    mkl_dcsrmm(&transa,&m2,&n,&k,&alpha,matdescra,mu_values,mu_cols,mu_row_offsets,&(mu_row_offsets[1]),forward,&n,&beta,forward_tmp,&n);

    //Swap vector values
    swap(forward, forward_tmp);


    // To avoid underflow, normalize
    //prob_t s = thrust::reduce(forward.begin(), forward.end());
    //thrust::transform(forward.begin(), forward.end(), forward.begin(), div_functor<prob_t>(s));
    //e += log(s);

  }

  prob_t result;
  //MULTIPLY HERE
  //cusparseSdoti
  if (verbose)
    cerr << "done" << endl;
  return log(result) + e;
}

int main (int argc, char *argv[]) {

  steady_clock::time_point clock1 = steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  input_fst t = read_fst(argv[1], inr, onr);

  steady_clock::time_point clock2 = steady_clock::now();
  duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;

  for (int iteration=0; iteration<1; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      steady_clock::time_point read_start = steady_clock::now();
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      prob_t final_prob = forward_mkl(t, input_symbols /*, fst_table*/);
      cout << "Forward probability: " << final_prob << endl;
      steady_clock::time_point read_end = steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }

 
  steady_clock::time_point clock3 = steady_clock::now();
  diff = clock3-clock2;
  cout << "Time to decode sentences: " << diff.count() << endl;

  return 0;
}
