// Parallel implementation of BFS
// concepts obtained from : Duane Merrill : High Performance and Scalable GPU Graph Traversal
// Program to perform BFS traversal from a given source vertex.

#include <iostream>
#include <set>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <list>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <thrust/scan.h>
#include <thrust/device_ptr.h>
#include <thrust/fill.h>
#include <cuda.h>
#include <thrust/sort.h>
#include <thrust/transform_reduce.h>
#include <thrust/sequence.h>
#include <thrust/device_vector.h>
#include <thrust/unique.h>
#include <thrust/count.h>
#include <thrust/device_vector.h>

#define DEBUG 1
#define MAX_VERTICES 4000000
//#define PROGRESS 1
#define NUM_BANKS 16
#define LOG_NUM_BANKS 4
#define CONFLICT_FREE_OFFSET(n) \
    ((n) >> NUM_BANKS + (n) >> (2 * LOG_NUM_BANKS)) 
#define BLOCK_SIZE 512

/// Code to compute the bit mask to check for (edges) / (vertex neighbors)
__device__ void  SetBit( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   A[i] = A[i] | flag;     // Set the bit at the k-th position in A[i]
}

__device__ void ClearBit( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)
   flag = ~flag;           // flag = 1111...101..111

   A[i] = A[i] & flag;     // RESET the bit at the k-th position in A[i]
}

__device__ int TestBit( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   if ( A[i] & flag )      // Test the bit at the k-th position in A[i]
      return 1;
   else
      return 0;
}

void  SetBit_host( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   A[i] = A[i] | flag;     // Set the bit at the k-th position in A[i]
}

void ClearBit_host( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)
   flag = ~flag;           // flag = 1111...101..111

   A[i] = A[i] & flag;     // RESET the bit at the k-th position in A[i]
}

int TestBitHost( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   if ( A[i] & flag )      // Test the bit at the k-th position in A[i]
      return 1;
   else
      return 0;
}

int bit_array_size(int original_size){
    int mod = original_size % 8;
    if(mod == 0){
        return int(original_size/8);
    }
    int bit_amount = original_size + (8 - (original_size % 8));
    return int(bit_amount/8);
}

__global__ void set_start_bit(int *bitMask,int index){
    SetBit(bitMask,index);
}

//Bit mask code section ends here


unsigned int round_off(unsigned int v){
    if (v > 1) 
    {
        float f = (float)v;
        unsigned int const t = 1U << ((*(unsigned int *)&f >> 23) - 0x7f);
        return (t << (t < v));
    }
    else 
    {
        return 1;
    }
}

__global__ void get_edge_frontier(int *neighbors,int *vertex_frontier,int *offset,int *d_c,int *d_r,int *size_neighbor,int *bitMask){
 
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    
    if(size_neighbor[0] > id){
            int end_index = d_r[vertex_frontier[id]];
            int start_index = d_r[vertex_frontier[id]-1];
	    int num_neighbors_expand = end_index-start_index;
	    // add correct amount of neighbors
            for(int i = 0; i< num_neighbors_expand; i++){
                //Fill the neighbor vector with neighbor id's
		if(bitMask[d_c[start_index + i]] == 0){
                    neighbors[offset[id]+i] = d_c[start_index + i];
		    bitMask[d_c[start_index + i]] = 1;
		}
		else{
		    neighbors[offset[id]+i] = MAX_VERTICES;
		}
            }
    }
}


__global__ void vertexFrontier(int *edgeFrontier,int *vertexFrontier,int *row_matrix,int *size_edge_frontier,int *bitMask,int *len)
{
    // Get our global thread ID
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    if(id < len[0]){
            // Make sure we do not go out of bounds
            edgeFrontier[id] = row_matrix[vertexFrontier[id]] - row_matrix[vertexFrontier[id]-1];
    }
}

__global__ void set_bit(int *bitMask,int val)
{
    bitMask[val] = 1;
}


__global__ void get_index_filter(int *neighbors,int *bitMask,int *index,int *len)
{
    // Get our global thread ID
    int id = blockIdx.x*blockDim.x+threadIdx.x;
 
    if(id < len[0]){
       if(neighbors[id] < MAX_VERTICES && neighbors[id] >0){
               bitMask[neighbors[id]] = 1;
        }
    }
}

void print_gpu_use(){
    size_t free_byte ;
    size_t total_byte ;
    cudaMemGetInfo( &free_byte, &total_byte ) ;
    double free_db = (double)free_byte ;
    double total_db = (double)total_byte ;
    double used_db = total_db - free_db ;
    printf("GPU memory usage: used = %f, free = %f MB, total = %f MB\n",used_db/1024.0/1024.0, free_db/1024.0/1024.0, total_db/1024.0/1024.0);
}

int sum(int *edge_frontier_host,int roundOff){
    int sum = 0;
    for(int i=0;i<roundOff;i++){
        sum+=edge_frontier_host[i];
    }
    return sum;
}
using namespace std;

// Driver program to test methods of graph class
int main(int argc, char* argv[])
{
    if (argc < 3) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
        cout << "Usage is a.out input_file\n"; // we need the csr format file containing the size of vector C and R
        exit(0);
    }

    
    char* myFile;
    int source_vertex;

    myFile = argv[1];
    source_vertex = (atoi(argv[2]));
    
    // Create a graph given the users specifications 
    ifstream csr_file;
    string line;
    csr_file.open (myFile);

    //Read "R" array
    getline(csr_file,line);
    stringstream buf(line);
    vector<string> tokens;
    while(!buf.eof()){
	string tmp;
	buf >> tmp;
	tokens.push_back(tmp);
    }
    int r_len = atoi(tokens[0].c_str());
   
    //create "C" array
    getline(csr_file,line);
    buf.clear();
    buf.str(line);
    vector<string> tokens2;
    while(!buf.eof()){
        string tmp;
        buf >> tmp;
        tokens2.push_back(tmp);
    }
    int c_len = atoi(tokens2[0].c_str());

    //Fill array "C"
    getline(csr_file,line);
    buf.clear();
    buf.str(line);
    vector<string> c;
 
    while(!buf.eof()){
        string tmp;
        buf >> tmp;
        c.push_back(tmp);
    }

    //Fill array "R"
    getline(csr_file,line);
    buf.clear();
    buf.str(line);
    vector<string> r;

    while(!buf.eof()){
        string tmp;
        buf >> tmp;
        r.push_back(tmp);
    }

    csr_file.close();

    
    int* c_array = new int[c_len];
    int* r_array = new int[r_len];


    // initialize the arrays
    for (int i = 0; i < c.size() ;i++){
        c_array[i] = atoi(c[i].c_str());
    }

    for (int i = 0; i < r.size(); i++){
        r_array[i] = atoi(r[i].c_str());
    }
  
    // Size, in bytes, of each vector
    size_t c_bytes = c_len * sizeof(int);
    size_t r_bytes = r_len * sizeof(int);
 
     // Device input vectors
     int *d_c;
     int *d_r;
     
     // Allocate memory for each vector on GPU
     cudaMalloc(&d_c, c_bytes);
     cudaMalloc(&d_r, r_bytes);

     // Copy host vectors to device
     cudaMemcpy( d_c, c_array, c_bytes, cudaMemcpyHostToDevice);
     cudaMemcpy( d_r, r_array, r_bytes, cudaMemcpyHostToDevice);
 
    /*Code to test the vertices explored by the algorithm*/
    int vertex_mask_size = 3566909;
    int *vertex_mask;
    cudaMalloc(&vertex_mask, sizeof(int)*vertex_mask_size);
    cudaMemset(vertex_mask, 0, sizeof(int)*vertex_mask_size);
    set_bit<<<1,1>>>(vertex_mask,source_vertex);

    int size_vertex_frontier = 1;
    int* size_vertex_frontier_device;
    cudaMalloc(&size_vertex_frontier_device, sizeof(int));
    cudaMemset(size_vertex_frontier_device, 1, sizeof(int));

    int* size_vertex_frontier_device_len;
    cudaMalloc(&size_vertex_frontier_device_len, sizeof(int));
    cudaMemset(size_vertex_frontier_device_len, 1, sizeof(int)); 

    int* vertex_frontier = new int[size_vertex_frontier];
    vertex_frontier[0] = source_vertex;
  
    int* vertex_device_size;
    std::set<int> neighbor_set;

    int* device_vertex_fr;
    cudaMalloc(&device_vertex_fr, vertex_mask_size*sizeof(int));
    cudaMemcpy( device_vertex_fr, vertex_frontier, size_vertex_frontier * sizeof(int), cudaMemcpyHostToDevice);

    cudaMalloc(&vertex_device_size, sizeof(int));
    cudaMemcpy(vertex_device_size, &size_vertex_frontier, sizeof(int), cudaMemcpyHostToDevice);

    int* size_edge_frontier;
    cudaMalloc(&size_edge_frontier, sizeof(int));
    cudaMemset(size_edge_frontier, 0, sizeof(int));


    int* edge_frontier_device;
    cudaMalloc(&edge_frontier_device, vertex_mask_size*sizeof(int));
    cudaMemset(edge_frontier_device, 0, vertex_mask_size*sizeof(int));

    // keep traversing while the vertex frontier is not empty

    while(size_vertex_frontier > 0){
        int* edge_frontier_size = new int[1];
       // Execute the kernel
	//printf("The size vertex frontier is: %d \n",size_vertex_frontier);
        dim3 grid_vertex((int)ceil((float)size_vertex_frontier/(float)1024),1), block_vertex(1024,1);
        #ifdef PROGRESS
            printf("START GETTING VERTEX FRONTIER \n");
        #endif

        vertexFrontier<<<grid_vertex, block_vertex>>>(edge_frontier_device,device_vertex_fr,d_r,size_edge_frontier,vertex_mask,vertex_device_size);

	/********** CODE TO DEBUG CAPACITY ********
		printf("-------------");
            int* neighbors_frontier = new int[size_vertex_frontier];
            cudaMemcpy(neighbors_frontier, device_vertex_fr,size_vertex_frontier*sizeof(int) , cudaMemcpyDeviceToHost );
            
            int stop = 0;
            //printf("Neighbors \n");
            for(int i=0;i<size_vertex_frontier;i++){
                if( neighbors_frontier[i] < 4000000){

                
                printf("%d\n",neighbors_frontier[i]);
                }
                if(neighbors_frontier[i] <= 0){
                        printf("NODE ERROR %d\n",i);
                        stop = 1;
                        //exit(-1);

                }
            }
            if(stop == 1){exit(-1);}
            delete[] neighbors_frontier;



	/******************************************/

        #ifdef PROGRESS
            printf("DONE GETTING VERTEX FRONTIER \n");
        #endif

        //after this point we have the total number of neighbors we can expand from thevertex frontier
        // we also have the total number of vertices on the edge frontier in "size_edge_frontier"

        // check for error
        cudaError_t vertex_frontier_error = cudaGetLastError();
        if(vertex_frontier_error != cudaSuccess)
        {
            printf("vertexFrontier: CUDA error: %s\n", cudaGetErrorString(vertex_frontier_error));
            exit(-1);
        }
 

	//Prefix scan using thrust libraries
        thrust::device_ptr<int> dev_ptr(edge_frontier_device);
        int d_sum = thrust::reduce(dev_ptr,dev_ptr+ int(size_vertex_frontier),(int) 0, thrust::plus<int>());
        thrust::exclusive_scan(dev_ptr, dev_ptr + int(size_vertex_frontier), dev_ptr);


        int* pointer_dev_cast = thrust::raw_pointer_cast(dev_ptr); 
        edge_frontier_size[0] = d_sum;

	//printf("The sum is: %d \n",d_sum);
        // If no neighbors were found, then we break
        if(d_sum <= 0){
            break;
        }
       // Allocate memory on the device for the potential edge frontier
        int* neighbors;
        cudaMalloc(&neighbors, edge_frontier_size[0]*sizeof(int)); 
        
	//Variable used to keep track of neighbors on the host device
        int* size_of_neighbors;
        cudaMalloc(&size_of_neighbors,sizeof(int));
        cudaMemcpy( size_of_neighbors,&size_vertex_frontier, sizeof(int), cudaMemcpyHostToDevice);
        

        dim3 grid_edges((int)ceil((float)size_vertex_frontier/(float)1024),1/*(int)ceil((float)size_vertex_frontier/(float)32)*/), block_edges(1024,1);

       	#ifdef PROGRESS
	    printf("START GET EDGE FRONTIER \n");
	#endif

        get_edge_frontier<<<grid_edges, block_edges>>>(neighbors,device_vertex_fr,edge_frontier_device,d_c,d_r,size_of_neighbors,vertex_mask);

	#ifdef PROGRESS
	    printf("END GET EDGE FRONTIER \n");
	#endif

	cudaError_t edges_frontier_error = cudaGetLastError();
        if(edges_frontier_error != cudaSuccess)
        {
            printf("edgesFrontier: CUDA error: %s\n", cudaGetErrorString(edges_frontier_error));
            exit(-1);
        }


        #ifdef PROGRESS
            printf("START THRUST \n");
        #endif

        thrust::device_ptr<int> neighbor_frontier_ptr(neighbors);

        #ifdef PROGRESS
            printf("CAST POINTER \n");
        #endif

//        thrust::sort(neighbor_frontier_ptr,neighbor_frontier_ptr+ int(edge_frontier_size[0]));


        #ifdef PROGRESS
            printf("ENDSORT \n");
/*****DEBUG SECTION**
printf("--------------------\n");
            int* neighbors_frontier = new int[edge_frontier_size[0]];
            cudaMemcpy(neighbors_frontier, neighbors,edge_frontier_size[0]*sizeof(int) , cudaMemcpyDeviceToHost );
            
            int stop = 0;
            //printf("Neighbors \n");
            for(int i=0;i<edge_frontier_size[0];i++){
                if( neighbors_frontier[i] <= 4000000){

                
                printf("%d\n",neighbors_frontier[i]);
                }
                if(neighbors_frontier[i] <= 0){
                        printf("NODE ERROR %d\n",i);
                        stop = 1;
                        exit(-1);

                }
            }
            if(stop == 1){exit(-1);}
            delete[] neighbors_frontier;
            printf("--------------------\n");
/******END DEBUG SECTION****/
        #endif

thrust::sort(neighbor_frontier_ptr,neighbor_frontier_ptr+ int(edge_frontier_size[0]));
        thrust::unique(neighbor_frontier_ptr,neighbor_frontier_ptr+ int(edge_frontier_size[0]));
       
        #ifdef PROGRESS
        
            printf("END THRUST \n");
        #endif

        int* single_elem_size;
        cudaMalloc(&single_elem_size, int(MAX_VERTICES)*sizeof(int));
        cudaMemset(single_elem_size,0, int(MAX_VERTICES)*sizeof(int));


        dim3 grid_neighbor_filter((int)ceil((float)edge_frontier_size[0]/(float)1024),1), block_neighbor_filter(1024,1);
        cudaMemset(size_vertex_frontier_device,0, sizeof(int));
        cudaMemcpy(size_vertex_frontier_device_len,edge_frontier_size, sizeof(int), cudaMemcpyHostToDevice);
        get_index_filter<<<grid_neighbor_filter, block_neighbor_filter>>>(neighbors,single_elem_size,size_vertex_frontier_device,size_vertex_frontier_device_len);


        cudaError_t vertex_filter_errorri = cudaGetLastError();
        if(vertex_filter_errorri != cudaSuccess)
        {
            printf("FilterFrontierrri: CUDA error: %s\n", cudaGetErrorString(vertex_filter_errorri));
            exit(-1);
        }


        thrust::device_ptr<int> single_elem_size2(single_elem_size);
        int single_elem = thrust::reduce(single_elem_size2,single_elem_size2+ int(MAX_VERTICES),(int) 0, thrust::plus<int>());

         
        cudaFree(single_elem_size);
        cudaMemset(device_vertex_fr, 0, vertex_mask_size*sizeof(int));
	cudaMemcpy(device_vertex_fr, neighbors, single_elem*sizeof(int) , cudaMemcpyDeviceToDevice );
        cudaMemcpy(vertex_device_size,&single_elem, sizeof(int), cudaMemcpyHostToDevice);
        cudaMemset(edge_frontier_device, 0, vertex_mask_size*sizeof(int));

/*****DEBUG SECTION**
            int* neighbors_frontier = new int[single_elem];
	    cudaMemcpy(neighbors_frontier, neighbors,single_elem*sizeof(int) , cudaMemcpyDeviceToHost );
	    
            int stop = 0;
            //printf("Neighbors \n");
     	    for(int i=0;i<single_elem;i++){
 	        if( neighbors_frontier[i] < 4000000){

                
                printf("%d\n",neighbors_frontier[i]);
                }
		if(neighbors_frontier[i] <= 0){
			printf("NODE ERROR %d\n",i);
                        stop = 1;
			//exit(-1);

		}
	    }
            if(stop == 1){exit(-1);}
	    delete[] neighbors_frontier;
	    printf("--------------------\n");
/******END DEBUG SECTION****/

        size_vertex_frontier = single_elem;
        delete[] edge_frontier_size;

        cudaFree(neighbors);
        cudaFree(size_of_neighbors);
    }

    
    int* vertex_printer = new int[vertex_mask_size];
    cudaMemcpy(vertex_printer, vertex_mask, vertex_mask_size*sizeof(int), cudaMemcpyDeviceToHost);
    for(int j = 0;j<vertex_mask_size;j++){
	if(vertex_printer[j] == 1){
            printf("%d\n",j);
	}
    }
    delete[] vertex_printer;

    cudaFree(size_vertex_frontier_device_len);
    cudaFree(edge_frontier_device);
    cudaFree(size_edge_frontier);
    cudaFree(vertex_device_size);
    cudaFree(device_vertex_fr);
    cudaFree(d_c);
    cudaFree(d_r);
    cudaFree(vertex_mask);
    cudaFree(size_vertex_frontier_device);
    delete[] c_array;
    delete[] r_array;
    return 0;
}
