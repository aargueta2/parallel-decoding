#include <iostream>
#include <set>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <list>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <thrust/scan.h>
#include <thrust/device_ptr.h>
#include <thrust/fill.h>
#include <cuda.h>
#include <thrust/transform_reduce.h>
#include <thrust/sequence.h>
#include <thrust/device_vector.h>
#include <thrust/unique.h>

#define DEBUG 1
#define PROGRESS 1
#define NUM_BANKS 16
#define LOG_NUM_BANKS 4
#define CONFLICT_FREE_OFFSET(n) \
    ((n) >> NUM_BANKS + (n) >> (2 * LOG_NUM_BANKS)) 
#define BLOCK_SIZE 512

/// Code to compute the bit mask to check for (edges) / (vertex neighbors)
__device__ void  SetBit( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   A[i] = A[i] | flag;     // Set the bit at the k-th position in A[i]
}

__device__ void ClearBit( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)
   flag = ~flag;           // flag = 1111...101..111

   A[i] = A[i] & flag;     // RESET the bit at the k-th position in A[i]
}

__device__ int TestBit( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   if ( A[i] & flag )      // Test the bit at the k-th position in A[i]
      return 1;
   else
      return 0;
}

void  SetBit_host( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   A[i] = A[i] | flag;     // Set the bit at the k-th position in A[i]
}

void ClearBit_host( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)
   flag = ~flag;           // flag = 1111...101..111

   A[i] = A[i] & flag;     // RESET the bit at the k-th position in A[i]
}

int TestBitHost( int *A,  int k )
{
   int i = k/32;
   int pos = k%32;

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   if ( A[i] & flag )      // Test the bit at the k-th position in A[i]
      return 1;
   else
      return 0;
}

int bit_array_size(int original_size){
    int mod = original_size % 8;
    if(mod == 0){
        return int(original_size/8);
    }
    int bit_amount = original_size + (8 - (original_size % 8));
    return int(bit_amount/8);
}

__global__ void set_start_bit(int *bitMask,int index){
    SetBit(bitMask,index);
}

//Bit mask code section ends here



unsigned int round_off(unsigned int v){
    if (v > 1) 
    {
        float f = (float)v;
        unsigned int const t = 1U << ((*(unsigned int *)&f >> 23) - 0x7f);
        return (t << (t < v));
    }
    else 
    {
        return 1;
    }
}

__global__ void get_edge_frontier(int *neighbors,int *vertex_frontier,int *offset,int *d_c,int *d_r,int *size_neighbor,int *bitMask){
 
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    
    if(size_neighbor[0] > id){
        if(bitMask[vertex_frontier[id]-1] == 0){
            
            int end_index = d_r[vertex_frontier[id]];
            int start_index = d_r[vertex_frontier[id]-1];
	    int num_neighbors_expand = end_index-start_index;
	    // add correct amount of neighbors
            for(int i = 0; i< num_neighbors_expand; i++){
                //Fill the neighbor vector with neighbor id's
                neighbors[offset[id]+i] = d_c[start_index + i];
            }
        }
	else{
            
        }
    }
}


__global__ void vertexFrontier(int *edgeFrontier,int *vertexFrontier,int *row_matrix,int *size_edge_frontier,int *bitMask,int *len,int *lock)
{
    // Get our global thread ID
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    //printf("The size of the vertex frontier is: %d and id is: %d and index %d and vertex_matrix %d \n",len[0],id,row_matrix[vertexFrontier[id]],vertexFrontier[id]);
    if(id < len[0]){
        if( bitMask[vertexFrontier[id]-1] == 0){
            //bitMask[vertexFrontier[id]-1] = 1;       
 
            // Make sure we do not go out of bounds
            edgeFrontier[id] = row_matrix[vertexFrontier[id]] - row_matrix[vertexFrontier[id]-1];
            //bitMask[vertexFrontier[id]-1] = 1;
        }
        else{
            //edgeFrontier[id-1] = 0;
        }
    }
}


/*This method only works for arrays of size 2^n where n>0*/
/*
__global__ void prescan(int *g_odata, int *g_idata, int n)
{
    extern __shared__ int temp[];  // allocated on invocation
    int thid = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = 1;
    //Below is A
    int ai = thid;
    int bi = thid+(n/2);

    int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    int bankOffsetB = CONFLICT_FREE_OFFSET(bi);
    temp[ai + bankOffsetA] = g_idata[ai];
    temp[bi + bankOffsetB] = g_idata[bi];

    //A end
    for (int d = n>>1; d > 0; d >>= 1)                    // build sum in place up the tree
    {
        __syncthreads();
        if (thid < d)
        {
            //Below is B
            int ai = offset * (2*thid + 1) - 1;
            int bi = offset * (2*thid + 2) - 1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            //B end
            temp[bi] += temp[ai];
        }
        offset *= 2;
    }
    //Below is C
    if(thid == 0) {temp[n - 1 + CONFLICT_FREE_OFFSET(n -1)] = 0;}
    //C end
    for (int d = 1; d < n; d *= 2) // traverse down tree & build scan
    {
        offset >>= 1;
        __syncthreads();
        if (thid < d)
        {
            //Below is D
            int ai = offset * (2*thid + 1) - 1;
            int bi = offset * (2*thid + 2) - 1;
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            //D end
            int t = temp[ai];
            temp[ai] = temp[bi];
            //printf("AI is: %d, offset is: %d, and thid is: %d \n",ai,offset,thid);
            //printf("BI is: %d, offset is: %d, and thid is: %d \n",bi,offset,thid);
            temp[bi] += t;
        }
    }
    __syncthreads();
    //Below is E
    g_odata[ai] = temp[ai + bankOffsetA];
    g_odata[bi] = temp[bi + bankOffsetB];
    //E end
}
*/

void print_gpu_use(){
    size_t free_byte ;
    size_t total_byte ;
    cudaMemGetInfo( &free_byte, &total_byte ) ;
    double free_db = (double)free_byte ;
    double total_db = (double)total_byte ;
    double used_db = total_db - free_db ;
    printf("GPU memory usage: used = %f, free = %f MB, total = %f MB\n",used_db/1024.0/1024.0, free_db/1024.0/1024.0, total_db/1024.0/1024.0);
}

int sum(int *edge_frontier_host,int roundOff){
    int sum = 0;
    for(int i=0;i<roundOff;i++){
        sum+=edge_frontier_host[i];
    }
    return sum;
}
using namespace std;

// Driver program to test methods of graph class
int main(int argc, char* argv[])
{
    if (argc < 3) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
        cout << "Usage is a.out input_file\n"; // we need the csr format file containing the size of vector C and R
        exit(0);
    }

    
    char* myFile;
    int source_vertex;

    myFile = argv[1];
    source_vertex = (atoi(argv[2]));
    
    // Create a graph given the users specifications 
    ifstream csr_file;
    string line;
    csr_file.open (myFile);

    //Read "R" array
    getline(csr_file,line);
    stringstream buf(line);
    vector<string> tokens;
    while(!buf.eof()){
	string tmp;
	buf >> tmp;
	tokens.push_back(tmp);
    }
    int r_len = atoi(tokens[0].c_str());
    ////cout << "LEGTH R = " << r_len << " and source: "<< source_vertex<<"\n";
   
    //create "C" array
    getline(csr_file,line);
    buf.clear();
    buf.str(line);
    vector<string> tokens2;
    while(!buf.eof()){
        string tmp;
        buf >> tmp;
        tokens2.push_back(tmp);
    }
    int c_len = atoi(tokens2[0].c_str());
    ////cout << "LENGTH C = "<< c_len <<"\n";

    //Fill array "C"
    getline(csr_file,line);
    buf.clear();
    buf.str(line);
    vector<string> c;
 
    while(!buf.eof()){
        string tmp;
        buf >> tmp;
        c.push_back(tmp);
    }

    //Fill array "R"
    getline(csr_file,line);
    buf.clear();
    buf.str(line);
    vector<string> r;

    while(!buf.eof()){
        string tmp;
        buf >> tmp;
        r.push_back(tmp);
    }

    csr_file.close();

    
    int* c_array = new int[c_len];
    int* r_array = new int[r_len];


    // initialize the arrays
    for (int i = 0; i < c.size() ;i++){
        c_array[i] = atoi(c[i].c_str());
        //std::coutd << c_array[i] << '\n';
    }

    for (int i = 0; i < r.size(); i++){
        r_array[i] = atoi(r[i].c_str());
        //std::cout << r_array[i] << '\n';
    }
  
    // Size, in bytes, of each vector
    size_t c_bytes = c_len * sizeof(int);
    size_t r_bytes = r_len * sizeof(int);
 
     // Device input vectors
     int *d_c;
     int *d_r;
     
     // Allocate memory for each vector on GPU
     cudaMalloc(&d_c, c_bytes);
     cudaMalloc(&d_r, r_bytes);

     // Copy host vectors to device
     cudaMemcpy( d_c, c_array, c_bytes, cudaMemcpyHostToDevice);
     cudaMemcpy( d_r, r_array, r_bytes, cudaMemcpyHostToDevice);
 
    /*Code to test the vertices explored by the algorithm*/
    int vertex_mask_size = 3566909;

    ////printf("The vertex mask size is: %d \n",vertex_mask_size);
    int *vertex_mask_host = new int[vertex_mask_size];
    memset (vertex_mask_host,0,vertex_mask_size*sizeof(int));
    vertex_mask_host[source_vertex] = 1;
 
    int *vertex_mask;
    cudaMalloc(&vertex_mask, sizeof(int)*vertex_mask_size);
    cudaMemset(vertex_mask, 0, sizeof(int)*vertex_mask_size);

    int size_vertex_frontier = 1;
    int* vertex_frontier = new int[size_vertex_frontier];
    vertex_frontier[0] = source_vertex;
  
    int* vertex_device_size;
    std::set<int> neighbor_set;
    // keep traversing while the vertex frontier is not empty
    while(size_vertex_frontier > 0){
        int* edge_frontier = new int[size_vertex_frontier];
        int* device_vertex_fr;
        int* edge_frontier_device;
	int frontier_bytes = size_vertex_frontier * sizeof(int);
        int* size_edge_frontier;
        int* edge_frontier_size = new int[1];
      
 
        cudaMalloc(&vertex_device_size, sizeof(int)); 
        cudaMemcpy(vertex_device_size, &size_vertex_frontier, sizeof(int), cudaMemcpyHostToDevice);

        cudaMalloc(&size_edge_frontier, sizeof(int));
        cudaMemset(size_edge_frontier, 0, sizeof(int));
        cudaMalloc(&device_vertex_fr, frontier_bytes);

        //int* edge_frontier_host = new int[int(size_vertex_frontier)];
        cudaMalloc(&edge_frontier_device, frontier_bytes);
        cudaMemset(edge_frontier_device, 0, frontier_bytes);


        for(int i=0;i<size_vertex_frontier;i++){
	    if(vertex_mask_host[vertex_frontier[i]] != 1){
		vertex_mask_host[vertex_frontier[i]] = 1;
	    }
        } 

        cudaMemcpy( device_vertex_fr, vertex_frontier, frontier_bytes, cudaMemcpyHostToDevice);

/*        
        #ifdef DEBUG
	    //printf("vertex frontier size: %d\n",size_vertex_frontier);
            for(int i=0;i<size_vertex_frontier;i++){
		//printf("%d ",vertex_frontier[i]);
            }
  	    //printf("\n\n\n");
        #endif
*/
        // Execute the kernel
        int* sum_mask = new int[int(size_vertex_frontier)];
        cudaMalloc(&sum_mask, frontier_bytes);
	cudaMemset(sum_mask, 0, frontier_bytes);


        dim3 grid_vertex((int)ceil((float)size_vertex_frontier/(float)1024),1/*(int)ceil((float)size_vertex_frontier/(float)32)*/), block_vertex(1024,1);
        
        #ifdef PROGRESS
            //printf("START GETTING VERTEX FRONTIER \n");
        #endif

        vertexFrontier<<<grid_vertex, block_vertex>>>(edge_frontier_device,device_vertex_fr,d_r,size_edge_frontier,vertex_mask,vertex_device_size,sum_mask);
        cudaFree(sum_mask);
	
        #ifdef PROGRESS
            //printf("DONE GETTING VERTEX FRONTIER \n");
        #endif

        //after this point we have the neighbor size for each of the vertices in the vertex frontier
        // we also have the total number of vertices on the edge frontier in "size_edge_frontier"

        cudaMemcpy(edge_frontier_size, size_edge_frontier, sizeof(int), cudaMemcpyDeviceToHost );
/*
        #ifdef DEBUG
            printf("THE EDGE FRONTIER SIZE IS: %d \n\n\n",edge_frontier_size[0]);
        #endif
  
        #ifdef PROGRESS
            printf("THE EDGE FRONTIER SIZE IS: %d \n\n\n",edge_frontier_size[0]);
        #endif
*/
        // check for error
        cudaError_t vertex_frontier_error = cudaGetLastError();
        if(vertex_frontier_error != cudaSuccess)
        {
            printf("vertexFrontier: CUDA error: %s\n", cudaGetErrorString(vertex_frontier_error));
            exit(-1);
        }


        #ifdef PROGRESS
	    //printf("START PREFIX \n");
	#endif
        thrust::device_ptr<int> dev_ptr(edge_frontier_device);
        int d_sum = thrust::reduce(dev_ptr,dev_ptr+ int(size_vertex_frontier),(int) 0, thrust::plus<int>());
        thrust::exclusive_scan(dev_ptr, dev_ptr + int(size_vertex_frontier), dev_ptr);


        int* pointer_dev_cast = thrust::raw_pointer_cast(dev_ptr);

	#ifdef PROGRESS       
	    //printf("END PREFIX \n");
	#endif
  
        
        edge_frontier_size[0] = d_sum;

        #ifdef DEBUG
            //printf("THE REAL EDGE FRONTIER SIZE IS: %d \n\n\n",d_sum);
        #endif
        #ifdef PROGRESS
	    //printf("THE REAL EDGE FRONTIER SIZE IS: %d \n\n\n",edge_frontier_size[0]);
 	#endif

        // Allocate memory on the device for the potential edge frontier
        int* neighbors;
        cudaMalloc(&neighbors, edge_frontier_size[0]*sizeof(int)); 
        
        int* size_of_neighbors;
        cudaMalloc(&size_of_neighbors,sizeof(int));
        cudaMemcpy( size_of_neighbors,&size_vertex_frontier, sizeof(int), cudaMemcpyHostToDevice);
        

        dim3 grid_edges((int)ceil((float)size_vertex_frontier/(float)1024),1/*(int)ceil((float)size_vertex_frontier/(float)32)*/), block_edges(1024,1);

       	#ifdef PROGRESS
	    //printf("START GET EDGE FRONTIER \n");
	#endif

        get_edge_frontier<<<grid_edges, block_edges>>>(neighbors,device_vertex_fr,edge_frontier_device,d_c,d_r,size_of_neighbors,vertex_mask);

	#ifdef PROGRESS
	    //printf("END GET EDGE FRONTIER \n");
	#endif

	cudaError_t edges_frontier_error = cudaGetLastError();
        if(edges_frontier_error != cudaSuccess)
        {
            printf("edgesFrontier: CUDA error: %s\n", cudaGetErrorString(edges_frontier_error));
            exit(-1);
        }

	int* neighbors_frontier = new int[edge_frontier_size[0]]; 

	cudaMemcpy(neighbors_frontier, neighbors, edge_frontier_size[0]*sizeof(int) , cudaMemcpyDeviceToHost );
     


/*****DELETE SECTION*****/ 
            int stop = 0;
            //printf("Neighbors \n");
     	    for(int i=0;i<edge_frontier_size[0];i++){
 	        //printf("%d\n",neighbors_frontier[i]);
                
		if(neighbors_frontier[i] <= 0){
			printf("NODE ERROR %d\n",i);
                        stop = 1;
			exit(-1);

		}
	    }
            if(stop == 1){exit(-1);}
/******END DELETE SECTION****/

        // check for error
        cudaError_t vertex_filter_error = cudaGetLastError();
        if(vertex_filter_error != cudaSuccess)
        {
            printf("FilterFrontier: CUDA error: %s\n", cudaGetErrorString(vertex_filter_error));
            exit(-1);
        }

 
  	delete[] vertex_frontier;

        // Allocate memory for the next iteration
	#ifdef PROGRESS
	    //printf("ADDING NEIGHBORS TO SET \n");      
	#endif

        for(int i = 0;i< edge_frontier_size[0];i++){
           if(vertex_mask_host[neighbors_frontier[i]] != 1){   
                if(neighbors_frontier[i]>0){
                    neighbor_set.insert(neighbors_frontier[i]);
                }
            }
        }

        vertex_frontier = new int[neighbor_set.size()];
	#ifdef PROGRESS
        //	printf("END ADDING NEIGHBORS TO SET \n");
	#endif



	#ifdef PROGRESS
          //  printf("ADDING NEIGHBORS TO FRONTIER\n");
	#endif

        int neighbors_frontier_len = 0;
	for (std::set<int>::iterator it=neighbor_set.begin(); it!=neighbor_set.end();it++){
     	    vertex_frontier[neighbors_frontier_len] = *it;
            printf("%d\n",vertex_frontier[neighbors_frontier_len]);
	    neighbors_frontier_len++;
        }

	#ifdef PROGRESS
            //printf("DONE ADDING NEIGHBORS TO FRONTIER \n");
	#endif

 	neighbor_set.clear();
        cudaError_t iteration_end_error = cudaGetLastError();
        if(iteration_end_error != cudaSuccess)
        {
            printf("Iteration End: CUDA error: %s\n", cudaGetErrorString(iteration_end_error));
            exit(-1);
        }

        size_vertex_frontier = neighbors_frontier_len;
        delete[] edge_frontier_size;
        delete[] edge_frontier;
	delete[] neighbors_frontier;

        cudaFree(neighbors);
        cudaFree(size_edge_frontier);
        cudaFree(device_vertex_fr);
        cudaFree(edge_frontier_device);
        cudaFree(size_of_neighbors);
        cudaFree(vertex_device_size);
    }
    cudaFree(d_c);
    cudaFree(d_r);
    cudaFree(vertex_mask);
    delete[] c_array;
    delete[] r_array;

    //cout << "DONE \n";
    return 0;
}
