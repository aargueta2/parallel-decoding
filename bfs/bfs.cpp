// Program to print BFS traversal from a given source vertex. BFS(int s) 
// traverses vertices reachable from s.
#include<iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <list>
#include <stdlib.h>
#include <fstream>
 
using namespace std;
 
// This class represents a directed graph using adjacency list representation
class Graph
{
    int V;    // No. of vertices
    list<int> *adj;    // Pointer to an array containing adjacency lists
public:
    Graph(int V);  // Constructor
    void addEdge(int v, int w); // function to add an edge to graph
    void BFS(int s);  // prints BFS traversal from a given source s
};
 
Graph::Graph(int V)
{
    this->V = V;
    adj = new list<int>[V];
}
 
void Graph::addEdge(int v, int w)
{
    //printf("EDGE BEGIN \n");
    adj[v].push_back(w); // Add w to v’s list.
    //printf("EDGE END \n");
}
 
void Graph::BFS(int s)
{
    // Mark all the vertices as not visited
    bool *visited = new bool[V];
    for(int i = 0; i < V; i++)
        visited[i] = false;
 
    // Create a queue for BFS
    list<int> queue;
 
    // Mark the current node as visited and enqueue it
    visited[s] = true;
    queue.push_back(s);
 
    // 'i' will be used to get all adjacent vertices of a vertex
    list<int>::iterator i;
 
    while(!queue.empty())
    {
        // Dequeue a vertex from queue and print it
        s = queue.front();
        cout << s << "\n";
        queue.pop_front();
 
        // Get all adjacent vertices of the dequeued vertex s
        // If a adjacent has not been visited, then mark it visited
        // and enqueue it
        for(i = adj[s].begin(); i != adj[s].end(); ++i)
        {
            if(!visited[*i])
            {
                visited[*i] = true;
		//cout << *i <<" ";
                queue.push_back(*i);
            }
        }
	//cout << "\n" ;
    }
}
 
// Driver program to test methods of graph class
int main(int argc, char* argv[])
{
    if (argc < 2) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
        cout << "Usage is a.out number_vertices input_file \n"; 
        exit(0);
    }

    char* myFile;
    int numVertex;


    
    numVertex = atoi(argv[1]);
    myFile = argv[2];
    //cout << numVertex << "\n";
    //cout << myFile << "\n";

    // Create a graph given the users specifications 
    Graph g(numVertex);

    ifstream csr_file;
    string line;
    csr_file.open (myFile);
    if (csr_file.is_open()){
	while(getline(csr_file,line)){
	    istringstream buf(line);
            istream_iterator<string> beg(buf), end;
            vector<string> tokens(beg, end);
            //cout << atoi(tokens[0].c_str()) << ' ' <<  atoi(tokens[1].c_str()) << '\n';
            g.addEdge(atoi(tokens[0].c_str()), atoi(tokens[1].c_str()));
        }
    }
    csr_file.close();

    //cout << "Following is Breadth First Traversal (starting from vertex " << "6"<<") \n";
    g.BFS(1);
    cout << '\n'; 
    return 0;
}
