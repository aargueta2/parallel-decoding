#include <fstream>
#include <string>
#include <chrono>
#include <cmath>

#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_fst.hpp"
#include "prob_ptr.hpp"
#define INITIAL_VAL -FLT_MAX

using namespace std;

int ceildiv(int x, int y) { return (x-1)/y+1; }
int threaddiv(int x,int y){
	if(x%2 == 0){//if odd length list
		return ceildiv(x,y)+1;
	}
	else{
		return (x/y)+1;
	}
}
__global__ void compute_initial(int *from_states, int *to_states, float *probs,
				int start_offset, int end_offset,
				state_t end_state,
				prob_t *backward)
{
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset && to_states[offset] == end_state) {
	atomicLogAdd(&backward[from_states[offset]], probs[offset]);
    }
}

__global__ void compute_transition_dynamic_stride(int *from_states, int *to_states, float *probs,
                                   int start_offset, int end_offset,
                                   float *backward_prev, float *backward,int state_num,
				   int offset, int next_stride_source, int prev_stride_source){
	int edge = blockIdx.x*blockDim.x+threadIdx.x;
	if(from_states[offset+edge] != next_stride_source && from_states[offset+edge] != prev_stride_source){
            if(backward_prev[to_states[offset+edge]] > INITIAL_VAL){
                logAdd(&backward[from_states[offset+edge]],(backward_prev[to_states[offset+edge]] + probs[offset+edge]));
            }
        }
        else{
            if(backward_prev[to_states[offset+edge]] > INITIAL_VAL){
                atomicLogAdd(&backward[from_states[offset+edge]],(backward_prev[to_states[offset+edge]] + probs[offset+edge]));
            }
        }
}

__global__ void compute_transition_dynamic(int *from_states, int *to_states, float *probs,
                                   int start_offset, int end_offset,
                                   float *backward_prev, float *backward,int state_num) {
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int stride_size = 6;
    __shared__ int middle_index[1];
    middle_index[0] = (end_offset-start_offset)/stride_size;
    int offset = start_offset+(stride_size*id);

    if(id < middle_index[0]){
        int next_stride_source = from_states[offset+stride_size];
        int prev_stride_source = from_states[offset-1];
	compute_transition_dynamic_stride<<<stride_size,1>>>(from_states,to_states,probs,start_offset,end_offset,backward_prev,backward,state_num,offset,next_stride_source,prev_stride_source);
    }
    else if(id == middle_index[0]){
        for(int edge = 0;edge < stride_size; edge++){
            if(offset+edge < end_offset){
                if(backward_prev[to_states[offset+edge]] > INITIAL_VAL){
                    atomicLogAdd(&backward[from_states[offset+edge]],(backward_prev[to_states[offset+edge]] + probs[offset+edge]));
                }
            }
        }
    }
}


__global__ void compute_transition_stride(int *from_states, int *to_states, float *probs,
                                   int start_offset, int end_offset,
                                   float *backward_prev, float *backward,int state_num) {
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int stride_size = 6;
    __shared__ int middle_index[1];
    middle_index[0] = (end_offset-start_offset)/stride_size;
    int offset = start_offset+(stride_size*id);

    if(id < middle_index[0]){
	int next_stride_source = from_states[offset+stride_size];
	int prev_stride_source = from_states[offset-1];
	for(int edge = 0;edge < stride_size; edge++){
		if(from_states[offset+edge] != next_stride_source && from_states[offset+edge] != prev_stride_source){
		    if(backward_prev[to_states[offset+edge]] > INITIAL_VAL){
                        atomicLogAdd(&backward[from_states[offset+edge]],(backward_prev[to_states[offset+edge]] + probs[offset+edge]));
                    }
		}
		else{
		    if(backward_prev[to_states[offset+edge]] > INITIAL_VAL){
		        atomicLogAdd(&backward[from_states[offset+edge]],(backward_prev[to_states[offset+edge]] + probs[offset+edge]));
		    }
		}
	}
    }
    else if(id == middle_index[0]){
	for(int edge = 0;edge < stride_size; edge++){
	    if(offset+edge < end_offset){
	        if(backward_prev[to_states[offset+edge]] > INITIAL_VAL){
		    atomicLogAdd(&backward[from_states[offset+edge]],(backward_prev[to_states[offset+edge]] + probs[offset+edge]));
                }
	    }
        }
    }
}

#define BLOCK_SIZE 192
__global__ void compute_transition_shared(int *from_states, int *to_states, float *probs,
                                   int start_offset, int end_offset,
                                   float *backward_prev, float *backward,int state_num) {
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    __shared__ int from_shared_states[BLOCK_SIZE];
    __shared__ int to_shared_states[BLOCK_SIZE];
    __shared__ float shared_probs[BLOCK_SIZE];
    int offset = start_offset+id;
    
    from_shared_states[threadIdx.x] = from_states[offset];
    to_shared_states[threadIdx.x] = to_states[offset];
    shared_probs[threadIdx.x] = probs[offset];

    if(offset < end_offset){
        if(backward_prev[to_shared_states[threadIdx.x]] > INITIAL_VAL){
	    atomicLogAdd(&backward[from_shared_states[threadIdx.x]],(backward_prev[to_shared_states[threadIdx.x]] + shared_probs[threadIdx.x]));
        }
    }
}

__global__ void compute_transition(int *from_states, int *to_states, float *probs,
				   int start_offset, int end_offset,
				   float *backward_prev, float *backward,int state_num) {
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if(offset < end_offset){
        if(backward_prev[to_states[offset]] > INITIAL_VAL){
            atomicLogAdd(&backward[from_states[offset]],(backward_prev[to_states[offset]] + probs[offset]));    
        }
    }
}


__global__ void get_path(int *from_nodes,
			 prob_ptr_t *backward,
			 int input_length, int num_nodes,
			 prob_ptr_t *path) {
  int id = blockIdx.x*blockDim.x+threadIdx.x;
  if (id == 0) {
      int state = unpack_ptr(path[input_length]);
      for (int t = input_length-1; t >= 0; t--) {
	prob_ptr_t pp = backward[t*num_nodes+state];
	path[t] = pp;
	state = from_nodes[unpack_ptr(pp)];
      }
  }
}

void backward(gpu_fst &m, const vector<sym_t> &input_symbols, vector<sym_t> &output_symbols) {
    int verbose=0;

    static thrust::device_vector<prob_t> backward;
    backward.resize(input_symbols.size() * m.num_states);
    thrust::fill(backward.begin(), backward.end(), INITIAL_VAL);

    thrust::device_vector<prob_ptr_t> path(input_symbols.size()+1);

    sym_t a = input_symbols[input_symbols.size()-1];
    int start_offset = m.input_offsets[a];
    int end_offset = m.input_offsets[a+1];

    compute_initial <<<ceildiv(end_offset-start_offset, 1024), 1024>>> (
	m.from_states.data().get(),
	m.to_states.data().get(),
	m.probs.data().get(),
	start_offset, end_offset,
	m.final_states[0],
	backward.data().get()+ ((input_symbols.size()-1)*m.num_states));

    if (verbose) {
      for (auto pp: backward)
	cout << unpack_prob(pp) << " ";
      cout << endl;
    }

    for (int t=input_symbols.size()-2; t>=0; t--) {
      sym_t a = input_symbols[t];
      int start_offset = m.input_offsets[a];
      int end_offset = m.input_offsets[a+1];
      if (verbose) {
	cerr << start_offset << " to " << end_offset << endl;
	for (int i=start_offset; i<end_offset; i++) {
	  cerr << m.from_states[i] << " " << m.to_states[i] << " " << m.probs[i] << endl;
	}
      }

/*
      compute_transition <<<ceildiv(end_offset-start_offset, 1024), 1024>>> (
          m.from_states.data().get(), 
	  m.to_states.data().get(),
	  m.probs.data().get(),
	  start_offset, end_offset,
          backward.data().get() + (t-1)*m.num_states,
          backward.data().get() + t*m.num_states);
*/

      //compute_transition_stride<<<ceildiv(ceildiv(end_offset-start_offset+6,6), 192), 192>>> ( //192 cores per multiprocessor Multiprocessors on the k40
      compute_transition <<<ceildiv(end_offset-start_offset, 192), 192>>>(
      //compute_transition_shared<<<ceildiv(end_offset-start_offset, 192), 192>>>(
      //compute_transition_dynamic<<<ceildiv(ceildiv(end_offset-start_offset+6,6), 192), 192>>> (
          m.from_states.data().get(), 
          m.to_states.data().get(),
          m.probs.data().get(),
          start_offset, end_offset,
          backward.data().get() + (t+1)*m.num_states,
          backward.data().get() + (t)*m.num_states,
	  m.num_states);
	  //threaddiv(end_offset-start_offset,2));

      if (verbose) {
	for (auto pp: backward)
	  cout << unpack_prob(pp) << " ";
	cout << endl;
      }
        cudaDeviceSynchronize();
        //printf("---------------------\n");
    }

    cudaError_t e = cudaGetLastError();                                 
    if (e != cudaSuccess) {                                              
      cerr << "CUDA failure: " << cudaGetErrorString(e) << endl;
      exit(1);
    }
/*
    thrust::host_vector<prob_ptr_t> h_path(path);
    output_symbols.resize(input_symbols.size());
    for (int t=0; t<input_symbols.size(); t++) {
      output_symbols[t] = m.outputs[unpack_ptr(h_path[t])];
    }
*/


/*
    for (int weights=0; weights<(input_symbols.size()* m.num_states); weights++) {
      if(h_weights[weights] !=INITIAL_VAL && h_weights[weights] != 0){
        printf("The weight is %f and index:%d \n",h_weights[weights],weights);
      }
    }
*/
}

int main (int argc, char *argv[]) {
  auto clock1 = std::chrono::steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  gpu_fst m = read_fst(argv[1], inr, onr);
  
  //for(int i = 0; i < m.final_states.size(); i++){
  //	printf("Final state is %d \n",int(m.final_states[i]));	
  //}

  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;

  for (int iteration=0; iteration<1000; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      auto read_start = std::chrono::steady_clock::now();
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      backward(m, input_symbols, output_symbols);
      auto read_end = std::chrono::steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }
  auto clock3 = std::chrono::steady_clock::now();
  diff = clock3-clock2;
  cout << "Total time to decode sentences: " << diff.count() << endl;

  return 0;
}
