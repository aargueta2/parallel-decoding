#include <fstream>
#include <string>
#include <chrono>
#include <cmath>

#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_fst.hpp"
#include "prob_ptr.hpp"
#define INITIAL_VAL -FLT_MAX

int ceildiv(int x, int y) { return (x-1)/y+1; }


__global__ void compute_initial_forward(int *from_states, int *to_states, float *probs,
				int start_offset, int end_offset,
				state_t initial_state,
				prob_t *forward)
{
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset && from_states[offset] == initial_state) {
	atomicLogAdd(&forward[to_states[offset]], probs[offset]);
    }
}

__global__ void compute_initial_backward(int *from_states, int *to_states, float *probs,
                                int start_offset, int end_offset,
                                state_t end_state,
                                prob_t *backward,prob_t *forward,prob_t *forward_current,prob_t *end_weight)
{
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset && forward[from_states[offset]] > INITIAL_VAL && to_states[offset] == end_state) {
        atomicLogAdd(&backward[from_states[offset]], probs[offset]);
	atomicLogAdd(&end_weight[0],forward_current[to_states[offset]]);
    }
}

__global__ void compute_forward_transition(int *from_states, int *to_states, float *probs,
				   int start_offset, int end_offset,
				   float *forward_prev, float *forward) {
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset) {
	if(forward_prev[from_states[offset]] > INITIAL_VAL){
		atomicLogAdd(&forward[to_states[offset]],(forward_prev[from_states[offset]] + probs[offset]));
	}
    }
}

__global__ void compute_backward_transition(int *from_states, int *to_states, float *probs,
                                   int start_offset, int end_offset,
                                   float *backward_prev, float *backward,float *forward) {
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset) {
        if(forward[to_states[offset]] > INITIAL_VAL/*backward_prev[to_states[offset]] > INITIAL_VAL*/){
                atomicLogAdd(&backward[from_states[offset]],(backward_prev[to_states[offset]] + probs[offset]));
        }
    }
}

__global__ void compute_final(int *final_states, float *final_probs,
			      int num_finals,
			      prob_t *forward_prev, prob_ptr_t *forward) {
  int id = blockIdx.x*blockDim.x+threadIdx.x;
  if (id < num_finals) {
    prob_ptr_t pp = pack(unpack_prob(forward_prev[final_states[id]]) + final_probs[id], final_states[id]);
    atomicMax(forward, pp);
  }
}


__global__ void compute_expected_count_transition(int *from_states, int *to_states, float *probs,
                                   int start_offset, int end_offset,
                                   float *forward, float *backward,float *expected_counts,float *end_forward) {

    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset) {
        if(forward[from_states[offset]] > INITIAL_VAL && backward[to_states[offset]] > INITIAL_VAL){
		expected_counts[offset] = forward[from_states[offset]] + probs[offset] + backward[to_states[offset]]- end_forward[0];	
        }
    }

}

using namespace std;
void forward(gpu_fst &m, const vector<sym_t> &input_symbols, vector<sym_t> &output_symbols) {
    int verbose=0;

    static thrust::device_vector<prob_t> forward;
    forward.resize(input_symbols.size() * m.num_states);
    thrust::fill(forward.begin(),forward.end(), INITIAL_VAL);

    static thrust::device_vector<prob_t> backward;
    backward.resize(input_symbols.size() * m.num_states);
    thrust::fill(backward.begin(),backward.end(), INITIAL_VAL);


    sym_t a = input_symbols[0];
    int start_offset = m.input_offsets[a];
    int end_offset = m.input_offsets[a+1];

    // FORWARD PASS BEGINS
    compute_initial_forward <<<ceildiv(end_offset-start_offset, 1024), 1024>>> (
	m.from_states.data().get(),
	m.to_states.data().get(),
	m.probs.data().get(),
	start_offset, end_offset,
	m.initial,
	forward.data().get());

    cudaDeviceSynchronize();

    if (verbose) {
      for (auto pp: forward)
	cout << unpack_prob(pp) << " ";
      cout << endl;
    }

    for (int t=1; t<input_symbols.size(); t++) {
      sym_t a = input_symbols[t];
      int start_offset = m.input_offsets[a];
      int end_offset = m.input_offsets[a+1];
      if (verbose) {
	cerr << start_offset << " to " << end_offset << endl;
	for (int i=start_offset; i<end_offset; i++) {
	  cerr << m.from_states[i] << " " << m.to_states[i] << " " << m.probs[i] << endl;
	}
      }

      compute_forward_transition <<<ceildiv(end_offset-start_offset, 1024), 1024>>> (
          m.from_states.data().get(), 
	  m.to_states.data().get(),
	  m.probs.data().get(),
	  start_offset, end_offset,
          forward.data().get() + (t-1)*m.num_states,
          forward.data().get() + t*m.num_states);
      if (verbose) {
	for (auto pp: forward)
	  cout << unpack_prob(pp) << " ";
	cout << endl;
      }
    }

    // FORWARD PASS ENDS
    cudaDeviceSynchronize();
    //BACKWARD PASS BEGINS
    static thrust::device_vector<prob_t> final_weight;
    final_weight.resize(1);
    thrust::fill(final_weight.begin(),final_weight.end(), INITIAL_VAL);

      a = input_symbols[input_symbols.size()-1];
      start_offset = m.input_offsets[a];
      end_offset = m.input_offsets[a+1];

    compute_initial_backward <<<ceildiv(end_offset-start_offset, 1024), 1024>>> (
        m.from_states.data().get(),
        m.to_states.data().get(),
        m.probs.data().get(),
        start_offset, end_offset,
        m.final_states[0],
        backward.data().get()+ ((input_symbols.size()-1)*m.num_states),
	forward.data().get()+ ((input_symbols.size()-2)*m.num_states),
	forward.data().get()+ ((input_symbols.size()-1)*m.num_states),
	final_weight.data().get());

    cudaDeviceSynchronize();   
    for (int t=input_symbols.size()-2; t>=0; t--) {
      sym_t a = input_symbols[t];
      int start_offset = m.input_offsets[a];
      int end_offset = m.input_offsets[a+1];
      if (verbose) {
          cerr << start_offset << " to " << end_offset << endl;
          for (int i=start_offset; i<end_offset; i++) {
              cerr << m.from_states[i] << " " << m.to_states[i] << " " << m.probs[i] << endl;
          }
        }
        compute_backward_transition <<<ceildiv(end_offset-start_offset, 1024), 1024>>> (
          m.from_states.data().get(),
          m.to_states.data().get(),
          m.probs.data().get(),
          start_offset, end_offset,
          backward.data().get() + (t+1)*m.num_states,
          backward.data().get() + (t)*m.num_states,
	  forward.data().get()+ ((t)*m.num_states));
	cudaError_t e = cudaGetLastError();
    }
    cudaDeviceSynchronize();
    //BACKWARD PASS ENDS
    cudaError_t e = cudaGetLastError();                                 
    if (e != cudaSuccess) {                                              
      cerr << "CUDA failure: " << cudaGetErrorString(e) << endl;
      exit(1);
    }

 
    if (verbose) {
        thrust::host_vector<prob_t> f_weights(forward);
        for (int weights=0; weights<(input_symbols.size()* m.num_states); weights++) {
            if(f_weights[weights] !=INITIAL_VAL && f_weights[weights] != 0){
      	        printf("The forward weight is %f and index:%d \n",f_weights[weights],weights);
            }
        }

        printf("_________________________\n");
        thrust::host_vector<prob_t> b_weights(backward);
        for (int weights=0; weights<(input_symbols.size()* m.num_states); weights++) {
          if(b_weights[weights] !=INITIAL_VAL && b_weights[weights] != 0){
            printf("The backward weight is %f and index:%d \n",b_weights[weights],weights);
          }
        }
    }
    cudaDeviceSynchronize();
    //EXPECTED-COUNTS SECTION
    static thrust::device_vector<prob_t> expected_counts;
    expected_counts.resize(int(m.to_states.size()));
    thrust::fill(expected_counts.begin(),expected_counts.end(), 0);

    for (int t=0; t<input_symbols.size(); t++) {
        sym_t a = input_symbols[t];
        int start_offset = m.input_offsets[a];
        int end_offset = m.input_offsets[a+1];
        compute_expected_count_transition<<<ceildiv(end_offset-start_offset, 1024), 1024>>>(
          m.from_states.data().get(),
          m.to_states.data().get(),
          m.probs.data().get(),
          start_offset, end_offset,
          forward.data().get() + (t)*m.num_states,
          backward.data().get() + t*m.num_states,
	  expected_counts.data().get(),
	  final_weight.data().get());
	    cudaError_t end2 = cudaGetLastError();
    if (end2 != cudaSuccess) {
      cerr << "CUDA failure22: " << cudaGetErrorString(end2) << endl;
      exit(1);
    }
    }
    cudaDeviceSynchronize();
    //END EXPECTED-COUNTS SECTION
    cudaError_t end = cudaGetLastError();
    if (end != cudaSuccess) {
      cerr << "CUDA failure2: " << cudaGetErrorString(end) << endl;
      exit(1);
    }
    cudaDeviceSynchronize();
}

int main (int argc, char *argv[]) {
 

  auto clock1 = std::chrono::steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  gpu_fst m = read_fst(argv[1], inr, onr);
  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;

  for (int iteration=0; iteration<1000; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      forward(m, input_symbols, output_symbols);
    }
  }
  cudaDeviceSynchronize();
  auto clock3 = std::chrono::steady_clock::now();
  diff = clock3-clock2;
  cout << "Time to compute forward_backward: " << diff.count() << endl;

  return 0;
}
