#include <fstream>
#include <string>
#include <chrono>
#include <cmath>

#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_fst.hpp"
#include "prob_ptr.hpp"
#define INITIAL_VAL -FLT_MAX
#define NUM_PARTS 1024
int ceildiv(int x, int y) { return (x-1)/y+1; }

float logadd(float x, float y) {
  if (x < y) std::swap(x, y);
  return x + log1p(exp(y-x));
}

__global__ void compute_initial(int *from_states, int *to_states, float *probs,
				int start_offset, int end_offset,
				state_t initial_state,
				prob_t *forward,int *iterations)
{
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset && from_states[offset] == initial_state) {
	atomicLogAdd_iter(&forward[to_states[offset]],probs[offset],iterations);
    }
}

__global__ void compute_transition(int *from_states, int *to_states, float *probs,
				   int start_offset, int end_offset,
				   float *forward_prev, float *forward,int num_states,int *iterations){
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset) {
        if(forward_prev[from_states[offset]] > INITIAL_VAL){
		atomicLogAdd(&forward[to_states[offset]],(forward_prev[from_states[offset]] + probs[offset]));
                //atomicLogAdd_iter(&forward[to_states[offset]],(forward_prev[from_states[offset]] + probs[offset]),iterations);
        }
    }
/*
    extern __shared__ int shared_data[];
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;
    if (offset < end_offset) {
	shared_data[num_states+to_states[offset]] = forward[to_states[offset]];
	shared_data[from_states[offset]] = forward_prev[from_states[offset]];
	__syncthreads();

	//I should be doing a reduction on the offset!
        for(unsigned int s =1; s<end_offset-start_offset; s*=2){
            int index = 2 * s * id;
            if(index < num_states && (index+s)<num_states){
                shared_data[index] += shared_data[index +s] ;
            }
            __syncthreads();
        }

	//REMOVE **
	if(forward_prev[from_states[offset]] > INITIAL_VAL){
		atomicLogAdd(&forward[to_states[offset]],(forward_prev[from_states[offset]] + probs[offset]));
		//printf("-The prob is: %f offset: %d val %f \n",(probs[offset]),offset,forward[to_states[offset]]);
	}
    }
*/
}

__global__ void compute_transition2(int *from_states, int *to_states, float *probs,
                                   int start_offset, int end_offset,
                                   float *forward_prev, float *forward,int num_states,int *iterations){
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    if (id < num_states) {
	for(int foo=start_offset;foo<end_offset;foo++){
		if(to_states[foo] == id && forward_prev[from_states[foo]] > INITIAL_VAL){
			if((forward_prev[from_states[foo]] + probs[foo]) >= forward[to_states[foo]]){
				forward[to_states[foo]] = (forward_prev[from_states[foo]] + probs[foo])+log1pf(expf(forward[to_states[foo]]-(forward_prev[from_states[foo]] + probs[foo])));
			}
			else{
				forward[to_states[foo]] = forward[to_states[foo]] + log1pf(expf((forward_prev[from_states[foo]] + probs[foo])-forward[to_states[foo]]));
			}
			//atomicLogAdd_iter(&forward[to_states[foo]],(forward_prev[from_states[foo]] + probs[foo]),iterations);
		}
	}

    }
}
__global__ void compute_final(int *final_states, float *final_probs,
			      int num_finals,
			      prob_t *forward_prev, prob_ptr_t *forward) {
  int id = blockIdx.x*blockDim.x+threadIdx.x;
  if (id < num_finals) {
    prob_ptr_t pp = pack(unpack_prob(forward_prev[final_states[id]]) + final_probs[id], final_states[id]);
    atomicMax(forward, pp);
  }
}

__global__ void get_path(int *from_nodes,
			 prob_ptr_t *forward,
			 int input_length, int num_nodes,
			 prob_ptr_t *path) {
  int id = blockIdx.x*blockDim.x+threadIdx.x;
  if (id == 0) {
      int state = unpack_ptr(path[input_length]);
      for (int t = input_length-1; t >= 0; t--) {
	prob_ptr_t pp = forward[t*num_nodes+state];
	path[t] = pp;
	state = from_nodes[unpack_ptr(pp)];
      }
  }
}

using namespace std;
void forward(gpu_fst &m, const vector<sym_t> &input_symbols, vector<sym_t> &output_symbols) {
    int verbose=0;

    static thrust::device_vector<prob_t> forward;
    forward.resize(input_symbols.size() * m.num_states);
    thrust::fill(forward.begin(),forward.end(), INITIAL_VAL);

    static thrust::device_vector<int> iterations;
    iterations.resize(1);
    thrust::fill(iterations.begin(),iterations.end(),0);
    //thrust::device_vector<prob_ptr_t> path(input_symbols.size()+1);

    sym_t a = input_symbols[0];
    int start_offset = m.input_offsets[a];
    int end_offset = m.input_offsets[a+1];

    compute_initial <<<ceildiv(end_offset-start_offset, 1024), 1024>>> (
	m.from_states.data().get(),
	m.to_states.data().get(),
	m.probs.data().get(),
	start_offset, end_offset,
	m.initial,
	forward.data().get(),
	iterations.data().get());


    if (verbose) {
      for (auto pp: forward)
	cout << unpack_prob(pp) << " ";
      cout << endl;
    }

    //cudaDeviceSynchronize();
    //printf("---------------------\n");
    for (int t=1; t<input_symbols.size(); t++) {
      sym_t a = input_symbols[t];
      int start_offset = m.input_offsets[a];
      int end_offset = m.input_offsets[a+1];
      if (verbose) {
	cerr << start_offset << " to " << end_offset << endl;
	for (int i=start_offset; i<end_offset; i++) {
	  cerr << m.from_states[i] << " " << m.to_states[i] << " " << m.probs[i] << endl;
	}
      }

      compute_transition<<<ceildiv(end_offset-start_offset, 192), 192/*ceildiv(end_offset-start_offset, 448), 448*/>>> (
          m.from_states.data().get(), 
	  m.to_states.data().get(),
	  m.probs.data().get(),
	  start_offset, end_offset,
          forward.data().get() + (t-1)*m.num_states,
          forward.data().get() + t*m.num_states,
	  m.num_states,
	  iterations.data().get());

      if (verbose) {
	for (auto pp: forward)
	  cout << unpack_prob(pp) << " ";
	cout << endl;
      }


    }


/*
    compute_final <<<ceildiv(m.final_states.size(), 1024), 1024>>> (
        m.final_states.data().get(),
	m.final_probs.data().get(),
	m.final_states.size(),
        forward.data().get() + (input_symbols.size()-1)*m.num_states,
        (&path.back()).get());
*/ 
   cudaError_t e = cudaGetLastError();                                 
    if (e != cudaSuccess) {                                              
      cerr << "CUDA failure: " << cudaGetErrorString(e) << endl;
      exit(1);
    }

/*
    thrust::host_vector<prob_ptr_t> h_path(path);
    output_symbols.resize(input_symbols.size());
    for (int t=0; t<input_symbols.size(); t++) {
      output_symbols[t] = m.outputs[unpack_ptr(h_path[t])];
    }
*/


/*
    thrust::host_vector<prob_t> h_weights(forward);

    prob_t final_prob = -FLT_MAX;
    int q,p;
    for (const auto &final: m.finals) {
      tie(q, p) = final;
      final_prob = logadd(final_prob, h_weights(input_symbols.size()-1 * q) + p);
    }
*/

/*
    printf("The weight is %f and index:%d \n",h_weights[int(m.final_states[0])+((input_symbols.size()-1)* (m.num_states))],int(m.final_states[0])+int((input_symbols.size()-1)* (m.num_states)));

    for (int weights=0; weights<(input_symbols.size()* m.num_states); weights++) {
      if(h_weights[weights] !=INITIAL_VAL && h_weights[weights] != 0){
      	printf("The forward weight is %f and index:%d \n",h_weights[weights],weights);
      }
    }
*/


}

int main (int argc, char *argv[]) {
 

  auto clock1 = std::chrono::steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  gpu_fst m = read_fst(argv[1], inr, onr);
  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;

  for (int iteration=0; iteration<1000; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      auto read_start = std::chrono::steady_clock::now();
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      forward(m, input_symbols, output_symbols);
      auto read_end = std::chrono::steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }
  auto clock3 = std::chrono::steady_clock::now();
  diff = clock3-clock2;
  cout << "Time to compute forward: " << diff.count() << endl;

  return 0;
}
