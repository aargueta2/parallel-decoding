# Parallel FST Algorithms #

This repository contains serial and parallel implementations of the Viterbi algorithm and other FST algorithms.

### Building ###

Example FST and data in :

     data/examples

| Items | Description                    |
| ------------- | ------------------------------ |
| `att_fst.fst.txt`      | Small FST in att format (fr-en)  |
| `att_fst.isym`   | Input Dictionary     |
| `att_fst.osym`   | Output Dictionary     |
| `all_100.txt`    | Input Sentences |

Baseline algorithms:

     make baselines

Parallel Viterbi:

     make viterbi

### Running ###

Baseline algorithms:

     bin/viterbi_serial data/examples/att_fst.{fst.txt,isym,osym} <input>
     bin/forward_serial data/examples/att_fst.{fst.txt,isym,osym} <input>
     bin/forward_cusparse data/examples/att_fst.{fst.txt,isym,osym} <input>
     bin/viterbi_openfst data/examples/att_fst.fst <input>

Parallel Viterbi:

     bin/viterbi data/examples/att_fst.{fst.txt,isym,osym} <input>

### References ###

* [ High-Performance and Scalable GPU Graph Traversal](http://dl.acm.org/citation.cfm?id=2717511)