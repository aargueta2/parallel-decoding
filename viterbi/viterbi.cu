#include <fstream>
#include <string>
#include <chrono>

#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_fst.hpp"
#include "prob_ptr.hpp"

using namespace std;

int ceildiv(int x, int y) { return (x-1)/y+1; }
#define BLOCK_SIZE 192
__global__ void compute_initial(int *from_states, int *to_states, float *probs,
				int start_offset, int end_offset,
				state_t initial_state,
				prob_ptr_t *viterbi)
{
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;

    __shared__ int from_shared_states[BLOCK_SIZE];
    __shared__ int to_shared_states[BLOCK_SIZE];
    __shared__ float shared_probs[BLOCK_SIZE];

    from_shared_states[threadIdx.x] = from_states[offset];
    to_shared_states[threadIdx.x] = to_states[offset];
    shared_probs[threadIdx.x] = probs[offset];

    if (offset < end_offset && from_shared_states[threadIdx.x] == initial_state) {
	atomicMax(&viterbi[to_shared_states[threadIdx.x]], pack(shared_probs[threadIdx.x], offset));
    }
}

__global__ void compute_transition(int *from_states, int *to_states, float *probs,
				   int start_offset, int end_offset,
				   prob_ptr_t *viterbi_prev, prob_ptr_t *viterbi) {
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    int offset = start_offset+id;

    __shared__ int from_shared_states[BLOCK_SIZE];
    __shared__ int to_shared_states[BLOCK_SIZE];
    __shared__ prob_ptr_t viterbi_from_shared_states[BLOCK_SIZE];
    __shared__ float shared_probs[BLOCK_SIZE];

    from_shared_states[threadIdx.x] = from_states[offset];
    to_shared_states[threadIdx.x] = to_states[offset];
    shared_probs[threadIdx.x] = probs[offset];
    viterbi_from_shared_states[threadIdx.x] = viterbi_prev[from_shared_states[threadIdx.x]];

/*
    register int from_register_state = from_states[offset];
    register int to_register_state = to_states[offset];
    register prob_ptr_t viterbi_from_register_state = viterbi_prev[from_register_state];
    register float register_prob = probs[offset];
*/
    if (offset < end_offset) {
   	//prob_ptr_t pp = pack(unpack_prob(viterbi_from_register_state) + register_prob, offset);
       prob_ptr_t pp = pack(unpack_prob(viterbi_from_shared_states[threadIdx.x]) + shared_probs[threadIdx.x], offset);
	atomicMax(&viterbi[/*to_register_state*/to_shared_states[threadIdx.x]], pp);
    }
}

__global__ void compute_final(int *final_states, float *final_probs,
			      int num_finals,
			      prob_ptr_t *viterbi_prev, prob_ptr_t *viterbi) {
  // To do: this should be a reduction instead
  int id = blockIdx.x*blockDim.x+threadIdx.x;
  if (id < num_finals) {
    prob_ptr_t pp = pack(unpack_prob(viterbi_prev[final_states[id]]) + final_probs[id], final_states[id]);
    atomicMax(viterbi, pp);
  }
}

__global__ void get_path(int *from_nodes,
			 prob_ptr_t *viterbi,
			 int input_length, int num_nodes,
			 prob_ptr_t *path) {
  int id = blockIdx.x*blockDim.x+threadIdx.x;
  if (id == 0) {
      int state = unpack_ptr(path[input_length]);
      for (int t = input_length-1; t >= 0; t--) {
	prob_ptr_t pp = viterbi[t*num_nodes+state];
	path[t] = pp;
	state = from_nodes[unpack_ptr(pp)];
      }
  }
}

//__device__ prob_ptr_t array[100];

prob_t viterbi(gpu_fst &m, const vector<sym_t> &input_symbols, vector<sym_t> &output_symbols) {
    int verbose=0;

    static thrust::device_vector<prob_ptr_t> viterbi;
    viterbi.resize(input_symbols.size() * m.num_states);
    thrust::fill(viterbi.begin(), viterbi.end(), pack(-FLT_MAX, 0));

    thrust::device_vector<prob_ptr_t> path(input_symbols.size()+1);

    sym_t a = input_symbols[0];
    int start_offset = m.input_offsets[a];
    int end_offset = m.input_offsets[a+1];

    compute_initial <<<ceildiv(end_offset-start_offset, BLOCK_SIZE), BLOCK_SIZE>>> (
	m.from_states.data().get(),
	m.to_states.data().get(),
	m.probs.data().get(),
	start_offset, end_offset,
	m.initial,
	viterbi.data().get());

    if (verbose) {
      for (auto pp: viterbi)
	cout << unpack_prob(pp) << " ";
      cout << endl;
    }

    for (int t=1; t<input_symbols.size(); t++) {
      sym_t a = input_symbols[t];
      int start_offset = m.input_offsets[a];
      int end_offset = m.input_offsets[a+1];

      if (verbose) {
	cerr << start_offset << " to " << end_offset << endl;
	for (int i=start_offset; i<end_offset; i++) {
	  cerr << m.from_states[i] << " " << m.to_states[i] << " " << m.probs[i] << endl;
	}
      }

      compute_transition <<<ceildiv(end_offset-start_offset, BLOCK_SIZE), BLOCK_SIZE>>> (
          m.from_states.data().get(), 
	  m.to_states.data().get(),
	  m.probs.data().get(),
	  start_offset, end_offset,
          viterbi.data().get() + (t-1)*m.num_states,
          viterbi.data().get() + t*m.num_states);

      if (verbose) {
	for (auto pp: viterbi)
	  cout << unpack_prob(pp) << " ";
	cout << endl;
      }
    }

    compute_final <<<ceildiv(m.final_states.size(), 1024), 1024>>> (
        m.final_states.data().get(),
	m.final_probs.data().get(),
	m.final_states.size(),
        viterbi.data().get() + (input_symbols.size()-1)*m.num_states,
        (&path.back()).get());

    get_path <<<1,1>>> (
	m.from_states.data().get(),
        viterbi.data().get(),
	input_symbols.size(), m.num_states,
	path.data().get());

    cudaError_t e = cudaGetLastError();                                 
    if (e != cudaSuccess) {                                              
      cerr << "CUDA failure: " << cudaGetErrorString(e) << endl;
      exit(1);
    }


    thrust::host_vector<prob_ptr_t> h_path(path);
    output_symbols.resize(input_symbols.size());
    for (int t=0; t<input_symbols.size(); t++) {
      output_symbols[t] = m.outputs[unpack_ptr(h_path[t])];
    }

    return unpack_prob(h_path.back());
}

int main (int argc, char *argv[]) {
  auto clock1 = std::chrono::steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);
  const numberizer onr = read_numberizer(argv[3]);
  gpu_fst m = read_fst(argv[1], inr, onr);
  
  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;


  for (int iteration=0; iteration< 1000; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      auto read_start = std::chrono::steady_clock::now();
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      prob_t final_prob = viterbi(m, input_symbols, output_symbols);
      cout << "Viterbi log-probability: " << final_prob << endl;
      cout << "Viterbi output: " << onr.join(output_symbols) << endl;
      auto read_end = std::chrono::steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }
  auto clock3 = std::chrono::steady_clock::now();
  diff = clock3-clock2;
  cout << "Time to decode sentences: " << diff.count() << endl;

  return 0;
}
