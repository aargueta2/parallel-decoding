/*
Testing shortest path algorithm on nvgraph
*/
#include <tuple>
#include <vector>
#include <map>
#include <chrono>
#include <algorithm>
#include <cfloat>
#include <cstdlib>

#include <cuda_runtime.h>
#include <cusparse.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include "numberizer.hpp"
#include "fst.hpp"

#include "nvgraph.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "gpu_utils.hpp"

using namespace std;

void check_status(nvgraphStatus_t status)
{
	if ((int)status != NVGRAPH_STATUS_SUCCESS)
	{
		printf("Status %d \n",NVGRAPH_STATUS_NOT_INITIALIZED);
  		printf("Status %d \n",NVGRAPH_STATUS_ALLOC_FAILED);
		printf("Status %d \n",NVGRAPH_STATUS_INVALID_VALUE);
 		printf("Status %d \n",NVGRAPH_STATUS_INTERNAL_ERROR);
		printf("ERROR : %d\n",status);
		exit(0);
	}
}

cusparseHandle_t cusparse_handle;

template<class T>
struct exp_functor {
  __device__ T operator()(T &x) const {
    return exp(x);
  }
};

template<class T>
struct div_functor {
  T y;
  div_functor(T y) : y(y) { }
  __device__ T operator()(T &x) const {
    return x/y;
  }
};

// FST whose transitions are indexed by input symbol, then "from" state
struct gpu_input_csr_fst {
  // This is just a big CSR matrix. The transitions for (q, r, f, *, *) are found
  // at M[f*num_states+q][r].
  state_t initial;
  thrust::device_vector<int> row_offsets;
  thrust::device_vector<state_t> to_states;
  thrust::device_vector<prob_t> probs;
  thrust::device_vector<state_t> final_states;
  thrust::device_vector<prob_t> final_probs;
  state_t final;
  state_t num_states;
  sym_t num_inputs, num_outputs;

  gpu_input_csr_fst (fst &&m)
    :
    initial(m.initial),
    num_states(m.num_states),
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    to_states(m.transitions.size()),
    probs(m.transitions.size()),
    final_states(m.finals.size()),
    final_probs(m.finals.size())
  {
    std::vector<int> h_row_offsets(num_inputs*num_states+1);

    // workaround for bug in nvcc
    sort_by_input_fromstate_tostate(m);

    int i = -1;
    for (int k=0; k<m.transitions.size(); k++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[k];
      while (i < f*num_states+q) {
        i++;
        h_row_offsets[i] = k;
      }
    }
    while (i < num_inputs*num_states) {
      i++;
      h_row_offsets[i] = m.transitions.size();
    }
    row_offsets =  h_row_offsets;

    // Unzip m.transitions into to_states and probs
    unzip_to_device<1>(m.transitions, to_states);
    unzip_to_device<4>(m.transitions, probs);
    //thrust::transform(probs.begin(), probs.end(), probs.begin(), exp_functor<prob_t>());
    
    final = int(std::get<0>(m.finals[0]));
    unzip_to_device<1>(m.finals, final_probs);
    //thrust::transform(final_probs.begin(), final_probs.end(), final_probs.begin(), exp_functor<prob_t>());
  }
};

prob_t forward_nvgraph(gpu_input_csr_fst &m, vector<sym_t> &input_symbols) {

  const int verbose = 0;

  thrust::device_vector<prob_t> forward(m.num_states, INFINITY);
  forward[0] = 0;
  thrust::device_vector<prob_t> forward_tmp(m.num_states,0);
  if (verbose) {
    cerr << "forward probabilities" << endl;
    for (const auto &p: forward)
      cerr << p << endl;
  }

  for (int t=0; t<input_symbols.size(); t++) {
 
  nvgraphHandle_t handle;
  nvgraphGraphDescr_t graph;
  nvgraphCSRTopology32I_t CSC_input;
  cudaDataType_t* edge_dimT;
  cudaDataType_t* vertex_dimT;
  const size_t vertex_numsets = 2,edge_numsets = 1;
  vertex_dimT = (cudaDataType_t*)malloc(vertex_numsets*sizeof(cudaDataType_t));
  edge_dimT = (cudaDataType_t*)malloc(vertex_numsets*sizeof(cudaDataType_t));
  vertex_dimT[0] = CUDA_R_32F;
  vertex_dimT[1] = CUDA_R_32F;
  edge_dimT[0] = CUDA_R_32F;
  edge_dimT[1] = CUDA_R_32F;
  CSC_input = (nvgraphCSRTopology32I_t) malloc(sizeof(struct nvgraphCSRTopology32I_st));

  check_status(nvgraphCreate(&handle));
  check_status(nvgraphCreateGraphDescr (handle, &graph));

    sym_t f = input_symbols[t];
    int nnz = m.row_offsets[(f+1)*m.num_states] - m.row_offsets[f*m.num_states];
    nnz = int(m.to_states.size());
    int * mu_row_offsets = m.row_offsets.data().get() + f*m.num_states;
    prob_t *mu_values = m.probs.data().get();
    int *mu_dest = m.to_states.data().get();
    float alpha = 0, beta = INFINITY;

    if (verbose) {
      cerr << "transition matrix" << endl;
      for (int i=0; i<m.num_states; i++) {
        cerr << "row " << i << " at offset " << m.row_offsets[f*m.num_states+i] << endl;
        for (int k=m.row_offsets[f*m.num_states+i]; k<m.row_offsets[f*m.num_states+i+1]; k++) {
          cerr << "  col " << m.to_states[k] << " val " << m.probs[k] << endl;
        }
      }
      cerr << "last row ends at offset " << m.row_offsets[(f+1)*m.num_states] << endl;
    }
 
    CSC_input->nvertices = int(m.num_states);
    CSC_input->nedges = nnz;
    CSC_input->destination_indices = mu_dest;
    CSC_input->source_offsets = mu_row_offsets;

    check_status(nvgraphSetGraphStructure(handle, graph, (void*)CSC_input, NVGRAPH_CSR_32));
    check_status(nvgraphAllocateVertexData(handle, graph, vertex_numsets, vertex_dimT));
    check_status(nvgraphAllocateEdgeData(handle, graph, edge_numsets, edge_dimT));
    check_status(nvgraphSetEdgeData(handle, graph, (void*)(mu_values), 0));
    check_status(nvgraphSetVertexData(handle, graph, forward.data().get(), 0));
    check_status(nvgraphSetVertexData(handle, graph, forward_tmp.data().get(), 1));
    check_status(nvgraphSrSpmv(handle,graph,0,&alpha,0,&beta,1,NVGRAPH_MIN_PLUS_SR));
    check_status(nvgraphGetVertexData(handle,graph,forward_tmp.data().get(),1));

    //forward_tmp[0] = INFINITY;

    if (verbose) {
	printf("\n\n");
        float *ptr_1 = (float*)malloc(m.num_states*sizeof(float));
        check_status(nvgraphGetVertexData(handle,graph,ptr_1,1));
        //ptr_1[0] = 0;
        for(int foo = 0; foo < m.num_states; foo++){
	    if(ptr_1[foo] != 0){
                printf("[%d] = %.10e\n",foo,ptr_1[foo]);
            }
        }
	free(ptr_1);
    }

    check_status(nvgraphDestroyGraphDescr(handle, graph));
    check_status(nvgraphDestroy(handle));
    free(edge_dimT); free(vertex_dimT); free(CSC_input);
    
    swap(forward, forward_tmp);

    if (verbose) {
      cerr << "forward probabilities" << endl;
      for (const auto &p: forward)
        cerr << p << endl;
    }
   
  }

  return (-1 * forward[m.final]);
}


int main(int argc, char **argv)
{

  auto clock1 = std::chrono::steady_clock::now();
  const numberizer inr = read_numberizer(argv[2]);

  const numberizer onr = read_numberizer(argv[3]);
  gpu_input_csr_fst t = read_fst_csc(argv[1], inr, onr,float(-1));
  auto clock2 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FST: " << diff.count() << endl;


  for (int iteration=0; iteration<1; iteration++) {
    vector<sym_t> input_symbols, output_symbols;
    ifstream infile(argv[4]);
    string line;
    while (getline(infile, line)) {
      auto read_start = std::chrono::steady_clock::now();
      input_symbols = inr.split(line);
      cout << "Input: " << inr.join(input_symbols) << endl;
      prob_t final_prob = forward_nvgraph(t, input_symbols);
      cout << "Viterbi probability: " << final_prob << endl;
      auto read_end = std::chrono::steady_clock::now();
      diff = read_end-read_start;
      cout << "Time to decode sentence: " << diff.count()  << endl;
    }
  }
  cudaDeviceSynchronize();
  auto clock3 = std::chrono::steady_clock::now();
  diff = clock3-clock2;
  cout << "Time to decode sentences: " << diff.count() << endl;

  return EXIT_SUCCESS;
}
