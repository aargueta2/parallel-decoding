# Convert FST from Carmel format to AT&T text format.

# To do: Carmel doesn't allow multiple final states, so it's common either to have
# epsilon transitions from pre-final states to the final state, or else to define
# a stop symbol (e.g., </s>) for the same purpose. We should provide a way to convert
# these to multiple final states.

import collections

class FST(object):
    def __init__(self):
        self.transitions = []
        self.initial = None
        self.finals = []

class EpsilonType(object):
    def __str__(self):
        return "<epsilon>"
Epsilon = EpsilonType()

def carmel_lexer(infile):
    state = 0
    chars = []

    def make_token(quoted=False):
        token = ''.join(chars)
        chars[:] = []
        if token == '*e*' and not quoted:
            return Epsilon
        else:
            return token

    while True:
        c = infile.read(1)
        if not c:
            break

        if state == 0:
            if c in ['(', ')']:
                yield c
            elif c.isspace():
                pass
            elif c == '"':
                state = 2
            else:
                chars.append(c)
                state = 1

        elif state == 1:
            if c == ')':
                yield make_token()
                yield c
                state = 0
            elif c.isspace():
                yield make_token()
                state = 0
            else:
                chars.append(c)

        elif state == 2:
            if c == '\\':
                state = 3
            elif c == '"':
                yield make_token(quoted=True)
                state = 0
            else:
                chars.append(c)

        elif state == 3:
            chars.append(c)
            state = 2

def read_carmel_from_file(infile):
    m = FST()

    final_state = int(infile.readline())
    m.finals.append((final_state, 1))

    tokens = carmel_lexer(infile)

    first = True
    while True:
        try:
            token = tokens.next()
        except StopIteration:
            break
        if token != '(':
            raise ValueError("expected (")
        q = int(tokens.next())
        if first:
            m.initial = q
            first = False
        while True:
            token = tokens.next()
            if token == ')':
                break
            if token != '(':
                raise ValueError("expected (")
            fields = []
            while True:
                token = tokens.next()
                if token == ')':
                    break
                fields.append(token)

            r = int(fields[0])
            p = float(fields[-1])
            if len(fields) == 3:
                f = e = fields[1]
            elif len(fields) == 4:
                f = fields[1]
                e = fields[2]
            else:
                raise ValueError("wrong number of fields in transition")

            m.transitions.append((q, r, f, e, p))
    return m

def write_att_to_file(m, fstfile):
    transitions = collections.defaultdict(list)
    for (q, r, f, e, p) in m.transitions:
        transitions[q].append((r, f, e, p))
    for q in transitions:
        transitions[q].sort()
    for (r, f, e, p) in transitions[m.initial]:
        fstfile.write("{} {} {} {} {}\n".format(m.initial, r, f, e, p))
    for q in transitions:
        if q != m.initial:
            for (r, f, e, p) in transitions[q]:
                fstfile.write("{} {} {} {} {}\n".format(q, r, f, e, p))
    for (q, p) in m.finals:
        fstfile.write("{} {}\n".format(q, p))

def write_symbols_to_file(m, mode, outfile):
    symbols = set()
    for (q, r, f, e, p) in m.transitions:
        if mode == 'i':
            symbols.add(f)
        elif mode == 'o':
            symbols.add(e)
        else:
            raise ValueError("mode must be 'i' or 'o'")
    outfile.write("{} 0\n".format(Epsilon))
    i = 1
    for s in symbols:
        if s == str(Epsilon):
            raise ValueError("symbol conflicts with epsilon")
        elif s == Epsilon:
            pass
        else:
            outfile.write("{} {}\n".format(s, i))
            i += 1

def read_carmel(filename_or_file):
    if isinstance(filename_or_file, str):
        with open(filename_or_file) as infile:
            return read_carmel_from_file(infile)
    elif isinstance(filename_or_file, file):
        return read_carmel_from_file(filename_or_file)
    else:
        raise TypeError()

def write_att(m, filename_or_file):
    if isinstance(filename_or_file, str):
        with open(filename_or_file, "w") as outfile:
            return write_att_to_file(m, outfile)
    elif isinstance(filename_or_file, file):
        write_att_to_file(m, filename_or_file)
    else:
        raise TypeError()

def write_symbols(m, mode, filename_or_file):
    if mode not in ['i', 'o']:
        raise ValueError("mode must be 'i' or 'o'")
    if isinstance(filename_or_file, str):
        with open(filename_or_file, "w") as outfile:
            return write_symbols_to_file(m, mode, outfile)
    elif isinstance(filename_or_file, file):
        write_symbols_to_file(m, mode, filename_or_file)
    else:
        raise TypeError()

if __name__ == "__main__":
    import sys
    infile = sys.argv[1]
    if infile == "-": infile = sys.stdin
    m = read_carmel(infile)
    prefix = sys.argv[2]
    write_att(m, prefix + ".fst.txt")
    write_symbols(m, 'i', prefix + ".isym")
    write_symbols(m, 'o', prefix + ".osym")
